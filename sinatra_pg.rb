#!/usr/bin/env ruby

require 'sinatra'
require 'pg'

db = PG::Connection.new(dbname: 'osm')

db.exec(%q{
    CREATE FUNCTION closest_point_wgs84(sg_wgs84 TEXT)
    RETURNS TABLE(lat FLOAT, lon FLOAT) AS $$
    <<block>>
    DECLARE
        search_geom GEOMETRY := ST_Transform(ST_GeomFromText(sg_wgs84, 4326),
                                             927700);
        result RECORD;
        min_dist FLOAT;
        bbox_union GEOMETRY;
        bbox_desired GEOMETRY;

        curs CURSOR FOR
        SELECT ST_Y(closest_point) lat,
               ST_X(closest_point) lon,
               closest_point_natgrid AS point,
               distance,
               bbox
        FROM (
            SELECT ST_Transform(closest_point_natgrid, 4326) AS closest_point,
                   closest_point_natgrid,
                   ST_Distance(block.search_geom, closest_point_natgrid)
                       AS distance,
                   bbox
            FROM (
                SELECT ST_ClosestPoint(linestring, block.search_geom)
                           AS closest_point_natgrid,
                       ST_Envelope(linestring) AS bbox
                FROM (
                    SELECT linestring
                    FROM way
                    ORDER BY linestring <-> block.search_geom
                ) AS linestring_table
            ) AS result_natgrid_table
        ) AS result_latlon_table;

    BEGIN
        OPEN curs;
        FETCH FIRST FROM curs INTO result;
        CLOSE curs;

        min_dist := result.distance;
        bbox_desired := ST_Buffer(result.point, min_dist);
        bbox_union := result.bbox;
        lat := result.lat;
        lon := result.lon;

        FOR result IN curs LOOP
            IF min_dist > result.distance THEN
                min_dist := result.distance;
                bbox_desired := ST_Buffer(result.point, min_dist);
                lat := result.lat;
                lon := result.lon;
            END IF;
            bbox_union := ST_Union(bbox_union, result.bbox);
            IF ST_Covers(bbox_union, bbox_desired) THEN
                EXIT;
            END IF;
        END LOOP;

        RETURN NEXT;
    END;
    $$ LANGUAGE plpgsql
})

db.prepare('closest_point_wgs84',
           'SELECT * FROM closest_point_wgs84($1)')

set :bind, "0.0.0.0"
set :port, 8463

get '/' do
	send_file 'public/grid_ajax.html'
end

find_point = proc do |lat,lon|
    rows = db.exec_prepared('closest_point_wgs84', ["POINT(#{lon} #{lat})"])
             .values
    if rows.size > 0
        rows[0].join ','
    else
        ''
    end
end

get '/find_point/*,*', &find_point
post '/find_point/*,*', &find_point

find_route = proc do |lat1,lon1,lat2,lon2|
    ""
end

get '/find_route/*,*/*,*', &find_route
post '/find_route/*,*/*,*', &find_route
