#!/usr/bin/env ruby

require 'sinatra/base'
require 'sqlite3'
require 'json'
require_relative 'osm/simple_grid'

class SinatraDb < Sinatra::Base

    set :bind, "0.0.0.0"
    set :port, 8464

    def initialize
        super
        @db = SQLite3::Database.new("data/db.sqlite3")
        @grid = SimpleGrid.new

        @grid2segment = @db.prepare("select way_id, node_index from grid2segment where x = ? and y = ?")
        @node2latlon = @db.prepare("select lat, lon from node where node_id = ?")
        @way2nodes = @db.prepare("select node_id from way2nodes where way_id = ? order by way_node_index")
    end

    def get_way_nodes(way_id, cache)
        if cache.has_key? way_id
            return cache[way_id]
        end
        result = @way2nodes.execute(way_id)
        nodes = []
        while row = result.next
            nodes << row[0]
        end
        cache[way_id] = nodes
    end

    def get_node_loc(node_id, cache)
        if cache.has_key? node_id
            return cache[node_id]
        end
        result = @node2latlon.execute(node_id)
        (row = result.next) or return cache[node_id] = nil
        cache[node_id] = row
    end

    get '/' do
        send_file 'public/grid_db.html'
    end

    post '/grid_info/*/*' do |lat, lon|
        square = @grid.grid_square(Vector[lat.to_f, lon.to_f])
        c1 = @grid.square_coords(square).to_a
        c2 = @grid.square_coords(square + Vector[1, 1]).to_a
        segments = []
        way_node_cache = {}
        node_loc_cache = {}
        p square
        result = @grid2segment.execute(square[0], square[1])
        while row = result.next
            (way_id, node_index) = row
            nodes = get_way_nodes(way_id, way_node_cache)
            loc1 = get_node_loc(nodes[node_index], node_loc_cache)
            loc2 = get_node_loc(nodes[node_index + 1], node_loc_cache)
            segments << [loc1, loc2]
        end
        JSON.generate [c1, c2, segments]
    end
    
    run! if __FILE__ == $0
end
