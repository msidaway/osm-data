#!/usr/bin/env ruby

require_relative 'osm/ways'

ways = Ways.new
ways.load_data
ways.init_grid
File.open("public/data/grid.js", "w") {|f| f.write ways.grid.to_js }
