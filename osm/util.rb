require 'matrix'

class Vector
    def adjusted_norm(other)
        lat = (self[0] + other[0]) / 2.0
        scaling = Math.cos(lat / 180.0 * Math::PI)
        diff = Vector.elements([
            self[0] - other[0],
            (self[1] - other[1]) * scaling
        ])
        diff.norm
    end
end
