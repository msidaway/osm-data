#!/usr/bin/env ruby

require 'set'
require 'matrix'
require 'sqlite3'
require_relative 'simple_grid'

class DbWays < SimpleGrid

    attr_accessor :db
    attr_reader :nodes, :ways, :ways_by_node

    def _way2nodes_set_dist
        @way2nodes_set_dist
    end

    def initialize(lat_dim=0.001, lon_dim=0.001*LngFactor)
        super(lat_dim, lon_dim)

        @db = SQLite3::Database.new("data/db.sqlite3")

        @grid2segment = @db.prepare("select way_id, node_index from grid2segment where x = ? and y = ?")
        @node2latlon = @db.prepare("select lat, lon from node where node_id = ?")
        @way2nodes = @db.prepare("select way_node_index, node_id, dist from way2nodes where way_id = ? order by way_node_index")
        @way2oneway = @db.prepare("select oneway from way where way_id = ?")
        @all_ways = @db.prepare("select way_id from way")
        @node2ways = @db.prepare("select distinct way_id from way2nodes where node_id = ?")
        @way2nodes_set_dist = @db.prepare("update way2nodes set dist = ? where way_id = ? and way_node_index = ?")

        @nodes = NodeContainer.new(self)
        @ways = WayContainer.new(self)
        @ways_by_node = WaysByNode.new(self)
    end

    def close
        @grid2segment.close
        @node2latlon.close
        @way2nodes.close
        @way2nodes_set_dist.close
        @way2oneway.close
        @node2ways.close
        @all_ways.close
        @db.close
    end

    class NodeContainer
        def initialize(parent); @parent = parent; end
        def [](node_id)
            @parent.get_node(node_id)
        end
    end

    class WayContainer
        def initialize(parent); @parent = parent; end
        def each(&block)
            @parent.each_way(&block)
        end
        def [](way_id)
            @parent.get_way(way_id)
        end
    end

    class WaysByNode
        def initialize(parent); @parent = parent; end
        def [](node_id)
            @parent.get_ways_by_node(node_id)
        end
    end

    Node = Struct.new("Node", :id, :vector)
    Way = Struct.new("Way", :id, :nodes, :oneway)

    class WayNode
        attr_reader :id, :dist
        def initialize(parent, way_id, way_node_index, node_id, dist)
            @parent = parent
            @way_id = way_id
            @way_node_index = way_node_index
            @id = node_id
            @dist = dist
        end
        def dist= value
            #puts "setting dist = #{value} for way_id #{@way_id} and way_node_index #{@way_node_index}"
            @parent._way2nodes_set_dist.execute(value, @way_id, @way_node_index)
            @dist = value
        end
        def inspect
            "<WayNode:@way_id=#{@way_id},@way_node_index=#{@way_node_index},@id=#{@id},@dist=#{@dist}>"
        end
    end

    def get_node(node_id)
        row = @node2latlon.execute(node_id).next
        if row
            Node.new node_id, Vector.elements(row)
        else
            nil
        end
    end

    def get_way(way_id)
        array = @way2nodes.execute(way_id).to_enum.map{|way_node_index,node_id,dist|
            WayNode.new(self, way_id, way_node_index, node_id, dist)
        }
        oneway = nil
        row = @way2oneway.execute(way_id).next
        if row
            oneway = row[0]
        end
        Way.new way_id, array, oneway
    end

    def each_way
        if block_given?
            @all_ways.execute.each{|row|
                yield row[0], get_way(row[0])
            }
        else
            to_enum(__method__)
        end
    end

    def get_ways_by_node(node_id)
        @node2ways.execute(node_id).to_enum.map{|row|
            way = get_way(row[0])
            [way, way.nodes.index{|wn| wn.id == node_id }]
        }
    end

    def [](index)
        array = []
        @grid2segment.execute(*index.to_a).each{|elem| array << elem }
        Set.new array
    end
end
