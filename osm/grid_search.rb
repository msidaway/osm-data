require 'matrix.rb'
require 'set.rb'
require_relative 'grid.rb'
require_relative 'plane_eqn.rb'
class GridSearch

    class Error < Exception; end
    class TooFarError < Error; end

    def initialize(grid)
        @grid = grid
    end

    def find_point(pos)
        inf = find_point_info(pos)
        inf && inf[:point]
    end

    # find closest point belonging to a road segment
    def find_point_info(pos, max_dist = 1/0.0)
        point = nil
        way = nil
        idx = nil
        dist = nil
        apos = @grid.point_to_apparent(pos)
        done_segs = Set.new
        frontiers = []
        queued_squares = Set.new
        segs = Set.new
        rects = Set.new
        grid_index = @grid.grid_square(pos)
        segdata = @grid[grid_index]
        queued_squares << grid_index
        max_dist /= 111257.79
        while true

            # highlight grid square
            rects << grid_index

            if segdata
                segdata.each{|inf|
                    (wayid, nidx) = inf
                    if done_segs.member? inf
                        next
                    end
                    done_segs << inf
                    #puts "#{wayid} #{nidx}"
                    seg = [@grid.nodes[@grid.ways[wayid].nodes[nidx].id].vector,
                           @grid.nodes[@grid.ways[wayid].nodes[nidx+1].id].vector]
                    segs << seg
                    (npoint, ndist) =
                        PlaneEqn.point_and_dist(@grid.seg_to_apparent(seg), apos)
                    #window.alert(pdist[0][0] + ", " + pdist[0][1]);
                    if dist.nil? || ndist < dist
                        way = wayid
                        idx = nidx
                        point = @grid.point_from_apparent(npoint)
                        dist = ndist
                    end
                }
            end
            pot_frontiers = [[grid_index[0]  ,grid_index[1]  ,1,0,   0,-1],
                             [grid_index[0]  ,grid_index[1]  ,0,1,  -1, 0],
                             [grid_index[0]  ,grid_index[1]+1,1,0,   0, 1],
                             [grid_index[0]+1,grid_index[1]  ,0,1,   1, 0]]
            pot_frontiers.each{|f|
                next_grid = Vector[grid_index[0]+f[4], grid_index[1]+f[5]]
                # don't queue a grid square more than once
                if queued_squares.member? next_grid
                    next
                end
                queued_squares << next_grid
                #window.alert(f);
                seg = [@grid.square_coords([f[0],      f[1]]),
                       @grid.square_coords([f[0]+f[2], f[1]+f[3]])]
                #window.alert(seg);
                (npoint, ndist) = PlaneEqn.point_and_dist(@grid.seg_to_apparent(seg),
                                                          apos)
                #window.alert(pdist);
                if !point || ndist < dist
                    frontiers << [ndist, next_grid]
                end
            }
            #window.alert(frontiers.map(function(a){return a[0];})+"");
            frontiers.sort! {|a, b| a[0] <=> b[0] }
            # N.B. remember we may have frontiers left over from last time
            # round, so we still need to check the distance
            if frontiers.size > 0 && (!point || frontiers[0][0] < dist)
                # cross the frontier
                f = frontiers.shift
                grid_index = f[1]
                segdata = @grid[grid_index]
            else
                break
            end
        end
        #mpg2.setLatLngs(rects);
        #mpl.setLatLngs(segs);
        #window.alert(rects.length);
        if dist > max_dist
            raise TooFarError.new
        end
        point && {:way_id => way, :idx => idx, :point => point}
    end
end
