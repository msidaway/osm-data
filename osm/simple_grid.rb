#!/usr/bin/env ruby

require 'matrix'
require_relative 'grid_iter'

class SimpleGrid
    include GridIter
    def initialize(lat_dim=0.001, lon_dim=0.001*LngFactor)
        @origin = Vector[51.5, 0.0]
        @dims = Vector[lat_dim, lon_dim]
    end
end
