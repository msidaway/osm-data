#!/usr/bin/env ruby

require 'nokogiri'
require 'matrix'
require_relative 'bsp'
require_relative 'grid'
require_relative 'tags'
require_relative 'constants'
require_relative 'util'

class Ways < Nokogiri::XML::SAX::Document

    class Error < Exception; end
    class NodeNotFoundError < Error
        def initialize(node)
            super("Referenced node not found: #{node}")
        end
    end

    include OSMConstants

    class Node
        attr_accessor :id, :vector, :visible
        def initialize(args = {})
            @id = args[:id]
            @vector = args[:vector]
            @visible = args[:visible]
        end
    end

    class Way
        attr_accessor :id, :name, :type, :oneway
        attr_reader :nodes

        class WayNodeArray < Array
            def []= (i, val)
                if val.is_a?(String)
                    raise "val.is_a?(String): #{val.inspect}"
                end
            end

            def push (val)
                if val.is_a?(String)
                    raise "val.is_a?(String): #{val.inspect}"
                end
            end

            alias :<< :push
        end

        def nodes= (nodes)
            nodes.each {|node|
                if node.is_a?(String)
                    raise "node.is_a?(String): #{node.inspect}"
                end
            }
            @nodes = WayNodeArray.new(nodes)
        end

        def initialize(args = {})
            @id = args[:id]
            @name = args[:name]
            @type = args[:type]
            @oneway = args[:oneway]
            @nodes = args[:nodes]
        end
    end

    class WayNode
        attr_accessor :id, :vector, :visible, :dist
        def initialize(node)
            @id = node.id
            @vector = node.vector
            @visible = node.visible
        end
    end

    attr_reader :nodes, :ways, :ways_by_name, :ways_by_node, :tree, :grid

    def start_element name, attributes = []
        @stack << name
        attrs = {}
        attributes.each{|k,v| attrs[k] = v }
        case name
            when "node"
                @nodes[attrs['id']] = Node.new(
                    id:      attrs['id'],
                    vector:  Vector[attrs['lat'].to_f, attrs['lon'].to_f],
                    visible: attrs['visible'] == 'true'
                )
            when "way"
                @wayid = attrs['id']
                @way_nodes = []
                @highway = nil
                @oneway = false
                @wayname = nil
            when "nd"
                @way_nodes << attrs['ref']
            when "tag"
                case attrs['k']
                    when "highway"
                        @highway = attrs['v']
                    when "oneway"
                        @oneway = Tags.convert('oneway', attrs['v'])
                    when "name"
                        @wayname = attrs['v']
                end
        end
    end

    def end_element name
        @stack.pop
        case name
            when "way"
                if HighwayTypes.member?(@highway) && !@ways.member?(@wayid)
                    way = Way.new(id:     @wayid,
                                  name:   @wayname,
                                  type:   @highway,
                                  oneway: @oneway,
                                  nodes:  @way_nodes)
                    @ways[@wayid] = way
                    if @wayname
                        @ways_by_name[@wayname] = way
                    end
                    @way_nodes.each_with_index{|node_id, idx|
                        @ways_by_node[node_id] << [way, idx]
                    }
                end
        end
    end

    def load_data
        @stack = []
        @nodes = {}
        @ways = {}
        @ways_by_name = {}
        @ways_by_node = Hash.new {|h,k| h[k] = [] }

        @parser = Nokogiri::XML::SAX::Parser.new(self)
        STDERR.puts "data/north.osm"
        @parser.parse(File.open('data/north.osm'))
        STDERR.puts "data/south_west.osm"
        @parser.parse(File.open('data/south_west.osm'))
        STDERR.puts "data/south_east.osm"
        @parser.parse(File.open('data/south_east.osm'))
        #puts @ways_by_name.keys

        @ways.each_value {|way|
            way.nodes = way.nodes.map {|ref|
                WayNode.new(@nodes[ref])
            }
            way.nodes.each_cons(2) {|wn1, wn2|
                wn1.dist = wn2.vector.adjusted_norm(wn1.vector)
            }
        }

        STDERR.puts "done!"
    end

    def process_data
        segments = []
        @ways.each_value{|way|
            way.nodes.each_index{|i|
                if !@nodes.has_key?(way.nodes[i])
                    raise NodeNotFoundError.new(way.nodes[i])
                end
                node = @nodes[way.nodes[i]]
                point = node.vector
                if i > 0
                    last_node = @nodes[way.nodes[i-1]]
                    point0 = last_node.vector
                    if point0 != point
                        segments << [point0, point, way]
                    end
                end
            }
        }
        @tree = BSPTree.new(segments)
        nil
    end

    def init_grid(*args)
        @grid = Grid.new(@ways, @nodes, *args)
        @grid.init_grid
    end

    def main
        load_data
        process_data
    end
end

if __FILE__ == $0
    Ways.new.main
end
