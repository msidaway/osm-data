#!/usr/bin/env ruby
module Tags
    Conversions = {
        'oneway' => 
            lambda {|key, val|
                [nil, "no", "false", "0"].member?(val) ? nil
              : ["-1", "reverse"].member?(val) ? -1 : 1
            }
    }

    def self.convert(key, value)
        conv = Conversions[key]
        if conv
            conv.call(key, value)
        else
            value
        end
    end

    def self.fetch(jdata, key)
        convert(key, jdata['tags'][key])
    end
end
