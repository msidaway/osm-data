module GridIter

    LngFactor = 1.6

    # convert point from lat/lon to apparent coordinates
    def point_to_apparent(point)
        Vector[point[0], point[1]/LngFactor]
    end

    def seg_to_apparent(seg)
        [point_to_apparent(seg[0]), point_to_apparent(seg[1])]
    end

    def point_from_apparent(point)
        Vector[point[0], point[1]*LngFactor]
    end

    def grid_square(point, &block)
        GridIter.grid_square(point, @origin, @dims)
    end

    def square_coords(index, &block)
        GridIter.square_coords(index, @origin, @dims)
    end

    def each_grid_index(a, b, &block)
        GridIter.each_grid_index(a, b, @origin, @dims, &block)
    end

    def self.grid_square(point, origin, dims)
        offset = (point - origin)
        index = Vector[(offset[0]/dims[0]).floor, (offset[1]/dims[1]).floor]
    end

    def self.square_coords(index, origin, dims)
        Vector[origin[0] + dims[0] * index[0], origin[1] + dims[1] * index[1]]
    end

    def self.each_grid_index(a, b, origin, dims)
        last = grid_square(b, origin, dims)
        index = grid_square(a, origin, dims)
        dir = b - a
        if dir.to_a.map(&:abs).max > 0
            dir = dir.normalize
        end
        while index != last
            yield(index)

            coords1 = square_coords(index, origin, dims).to_a
            coords2 = square_coords(index + Vector[1, 1], origin, dims).to_a

            dists = []
            if a[0] != b[0]
                dists << [(coords1[0] - a[0]) / (b[0] - a[0]), Vector[-1, 0]]
                dists << [(coords2[0] - a[0]) / (b[0] - a[0]), Vector[ 1, 0]]
            end
            if a[1] != b[1]
                dists << [(coords1[1] - a[1]) / (b[1] - a[1]), Vector[0, -1]]
                dists << [(coords2[1] - a[1]) / (b[1] - a[1]), Vector[0,  1]]
            end
            dists.sort! {|a,b| a[0] <=> b[0] }
            inc = dists[1][1]
            if dists.size == 4
                if dists[1][0] == dists[2][0]
                    # enter/exit at same corner
                    inc = dists[1][1] + dists[2][1]
                    if dir[0] * inc[0] < 0
                        inc = Vector[0, inc[1]]
                    end
                    if dir[1] * inc[1] < 0
                        inc = Vector[inc[0], 0]
                    end
                    # assert inc[0] != 0 || inc[1] != 0
                elsif dists[2][0] == dists[3][0]
                    inc = dists[2][1] + dists[3][1]
                    # bias towards positive - because of inclusive bounds here
                    if inc == Vector[-1, 1]
                        inc = Vector[0, 1]
                    elsif inc == Vector[1, -1]
                        inc = Vector[1, 0]
                    end
                else
                    inc = dists[2][1]
                end
            end
            index = Vector[index[0] + inc[0], index[1] + inc[1]]
        end
        yield(index)
    end

end
