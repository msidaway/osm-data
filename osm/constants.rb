#!/usr/bin/env ruby

require 'nokogiri'
require 'matrix'
require_relative 'bsp'
require_relative 'grid'
require_relative 'tags'

module OSMConstants

    HighwayTypes = %w{
        access
        bus_stop
        living_street
        mini_roundabout
        motorway
        motorway_junction
        motorway_link
        primary
        primary_link
        residential
        road
        secondary
        secondary_link
        service
        services
        tertiary
        tertiary_link
        trunk
        trunk_link
        turning_circle
        unclassified
    }

end
