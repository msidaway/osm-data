require 'set'
require_relative 'ways'

class Route

    class Error < Exception; end
    class TooFarError < Error; end

    def initialize(ways)
        @ways = ways
    end

    class SearchPosition

        class Tip
            attr_reader :dist, :node_id, :prev

            def initialize(dist, node_id, prev=nil)
                @dist = dist
                @node_id = node_id
                @prev = prev
            end

            def <=> (other)
                if @dist > other.dist
                     1
                elsif @dist < other.dist
                    -1
                else
                     0
                end
            end

            def to_a
                a = (@prev && @prev.to_a) || []
                a << @node_id
            end

            def inspect
                "<Tip:#{@dist},#{to_a.inspect}>"
            end
        end

        def add_tip_raw(new_dist, node_id, prev)
             tip = @visited[node_id]
             dist = tip && tip.dist
             if !dist || new_dist < dist
                tip = Tip.new(new_dist, node_id, prev)
                #p tip
                #sleep 1
                @visited[node_id] = tip
                @tips << tip
             end
        end

        def initialize(parent, ways, way, idx, param)
            @ways_by_node = ways.ways_by_node
            @nodes = ways.nodes
            @visited = {}
            @tips = SortedSet.new
            parent.endpoint_dists(way, idx, param).each{|dist,node_id|
                add_tip_raw dist, node_id, nil
            }
        end

        def add_tip dist, ni2, prev
            add_tip_raw prev.dist + dist, ni2, prev
        end

        def expand
            if @tips.size == 0
                #puts "@tips is empty"
                return false
            end
            tip = @tips.first
            @tips.delete(tip) # why no #shift?
            n1 = @nodes[tip.node_id]
            # generate new candidate node ids, and call add_tip
            @ways_by_node[tip.node_id].each{|way,idx|
                if idx > 0 && way.oneway != 1
                    wn = way.nodes[idx-1]
                    add_tip wn.dist, wn.id, tip
                end
                if idx < way.nodes.size - 1 && way.oneway != -1
                    add_tip way.nodes[idx].dist, way.nodes[idx+1].id, tip
                end
            }
            true
        end

        def radius_at_least? value
            @tips.size == 0 || value <= @tips.first.dist
        end

        def reached? node, extra=0
            tip = @visited[node]
            tip && radius_at_least?(tip.dist + extra)
        end

        def route(node)
            tip = @visited[node]
            tip && tip.to_a
        end

        def min_route(edists)
            edists.flat_map{|dist, node|
                tip = @visited[node]
                if tip
                    # N.B.: first layer is flattened
                    [[dist + tip.dist, tip.to_a]]
                else
                    []
                end
            }.min[1]
        end
    end

    def endpoint_dists(way, idx, param, ending=false)
        ni1 = way.nodes[idx].id
        ni2 = way.nodes[idx+1].id
        n1 = @ways.nodes[ni1]
        n2 = @ways.nodes[ni2]
        if param == 0
            [[0, ni1]]
        elsif param == 1
            [[0, ni2]]
        else
            dist = (n2.vector - n1.vector).norm
            list = [[dist*param, ni1], [dist*(1-param), ni2]]
            if way.oneway
                list.delete_at((ending == (way.oneway == 1)) ? 1 : 0)
            end
            list
        end
    end

    def param_for(args={})
        way = @ways.ways[args[:way_id]]
        n1v = @ways.nodes[way.nodes[args[:idx]].id].vector
        if args[:idx]+1 < way.nodes.size
            n2v = @ways.nodes[way.nodes[args[:idx]+1].id].vector
            (args[:point]-n1v).norm/(n2v-n1v).norm
        else
            # it's the last node in the way - param must be 0
            0
        end
    end

    def route(start, finish, max_dist=1/0.0)
        if start.is_a? Array
            (way1, idx1, param1) = start
        else
            (way1, idx1, param1) = [@ways.ways[start[:way_id]], start[:idx], param_for(start)]
        end
        if finish.is_a? Array
            (way2, idx2, param2) = finish
        else
            (way2, idx2, param2) = [@ways.ways[finish[:way_id]], finish[:idx], param_for(finish)]
        end
        max_dist /= 111257.79
        pos = SearchPosition.new(self, @ways, way1, idx1, param1)
        edists = endpoint_dists(way2, idx2, param2, true)
        found = false
        while !(found = edists.any?{|dist, node_id| pos.reached?(node_id, dist) })
            if !pos.expand
                #puts "Can't expand"
                break
            end
            if pos.radius_at_least?(max_dist)
                raise TooFarError.new
            end
        end
        found && pos.min_route(edists)
    end

    def get_points_for_route(start_point, finish_point, node_ids)
        points = node_ids.map{|node_id|
            @ways.nodes[node_id].vector
        }
        if points[0] != start_point
            points.unshift(start_point)
        end
        if points[-1] != finish_point
            points << finish_point
        end
        points
    end

    def route_points(a, b, max_dist=1/0.0)
        r = route(a, b, max_dist)
        if r
            get_points_for_route(a[:point], b[:point], r)
        else
            nil
        end
    end
end
