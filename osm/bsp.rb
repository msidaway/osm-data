require 'set'
require 'matrix'

class Numeric
    def isnan
        self != self
    end
end

class Vector
    def reverse
        Vector.elements self.to_a.reverse
    end
end

class BSPTree

    attr_reader :left, :right

    @@rot = Matrix[[0,1],[-1,0]]

    def line_eq(p1, p2)
        norm = (@@rot * (p2 - p1)).normalize
        dist = norm.inner_product p1
        return [norm, dist]
    end

    def intersect(norm1, dist1, norm2, dist2)
        mat = Matrix[norm1, norm2]
        vec = Vector[dist1, dist2]
        begin
            mat.inverse * vec
        rescue ExceptionForMatrix::ErrNotRegular
            p mat
            raise
        end
    end

    def initialize(segments)
        @segments = segments

        #puts @segments.size

        points = Set.new
        @segments.each{|p1,p2|
            points << p1 << p2
        }
        points = points.to_a

        # find splitting plane
        centroid = points.inject(&:+)/points.size
        dists = points.map{|p| (p - centroid).norm }
        begin
            idx = dists.each_with_index.min.last
        rescue ArgumentError => e
            #puts "points is #{points}, dists is #{dists}"
            raise e
        end
        closest = points[idx]
        #puts "centroid=#{centroid}, closest point=#{closest}" #", dists=#{dists}"
        @splitter = @segments.find{|segment| [segment[0], segment[1]].member? closest }

        (@norm, @dist) = line_eq(@splitter[1], @splitter[0])

        #puts "norm=#{@norm}, dist=#{@dist}, other_dist=#{@norm.inner_product @splitter[1]}"

        @left = nil
        @right = nil
        if @segments.size > 1
            #puts "splitter=#{@splitter}"
            left_segments = []
            right_segments = []
            @segments.each{|segment|
                next if segment.equal? @splitter
                (p1, p2, way) = segment
                side1 = @norm.inner_product(p1) - @dist
                side2 = @norm.inner_product(p2) - @dist
                if side1 >= 0 && side2 >= 0
                    right_segments << segment
                elsif side1 <= 0 && side2 <= 0
                    #break
                    left_segments << segment
                else
                    (norm, dist) = line_eq(p1, p2)
                    if norm == @norm || norm == -1 * @norm
                        #puts "Attempt to split line by itself"
                        right_segments << segment # can't split the line by itself
                    else

                        crossover = intersect(norm, dist, @norm, @dist)

                        #puts "1: #{p1} - dist #{side1}, 2: #{p2} - dist #{side2}"
                        dist = @norm.inner_product(crossover) - @dist
                        #puts "Crossover point: #{crossover} - dist #{dist}"
                        #break
                        seg1 = [p1, crossover, way]
                        seg2 = [crossover, p2, way]
                        if crossover[0].isnan || crossover[1].isnan
                            puts "crossover has NaNs: #{p1}, #{p2}, #{norm}, #{dist}, #{@norm}, #{@dist}"
                        end
                        if side1 > side2
                            (seg1, seg2) = [seg2, seg1]
                        end
                        if seg1[0] != seg1[1]
                            left_segments << seg1
                        end
                        if seg2[0] != seg2[1]
                            right_segments << seg2
                        end
                        #if seg1[0] != seg1[1] && seg2[0] != seg2[1]
                        #    puts "Both segments are okay after split"
                        #elsif seg1[0] == seg1[1] && seg2[0] == seg2[1]
                        #    puts "Both segments are empty after split"
                        #else
                        #    puts "One segment is empty after split"
                        #end

                    end
                end
            }
            if !left_segments.empty?
                @left = BSPTree.new(left_segments)
            end
            if !right_segments.empty?
                @right = BSPTree.new(right_segments)
            end
        end
    end

    def size
        1 + (@left ? @left.size : 0) + (@right ? @right.size : 0)
    end

    def depth
        1 + ([0] + (@left ? [@left.depth] : []) + (@right ? [@right.depth] : [])).max
    end

    def inspect
        "<BSPTree:#{@splitter}" +
            (@left ? ",@left(#{@left.depth},#{@left.size})" : "") +
            (@right ? ",@right(#{@right.depth},#{@right.size})" : "") + ">"
    end

    def to_json
        '{"plane": ['+@norm[0].to_s+','+@norm[1].to_s+','+@dist.to_s+']'+
            (@left ? (', "left": ' + @left.to_json) : "")+
            (@right ? (', "right": ' + @right.to_json) : "")+'}'
    end
end
