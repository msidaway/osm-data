require 'set'
require_relative 'grid_iter'

class Grid < Hash

    include GridIter

    attr_reader :origin, :nodes, :ways

    def lat_dim; @dims[0]; end
    def lon_dim; @dims[1]; end

    def update_index_bounds(index)
        @lat_min = [@lat_min, index[0]].min
        @lon_min = [@lon_min, index[1]].min
        @lat_max = [@lat_max, index[0]].max
        @lon_max = [@lon_max, index[1]].max
    end

    def each_grid(a, b)
        each_grid_index(a, b) {|index|
            yield(index, self[index])
        }
    end

    def each_square(a, b)
        each_grid_index(a, b) {|index|
            yield(self[index])
        }
    end

    def initialize(ways, nodes, lat_dim=0.001, lon_dim=0.001*LngFactor)
        self.default_proc = proc {|h,k| h[k] = Set.new }
        @origin = ways.values.first.nodes.first.vector
        @ways = ways
        @nodes = nodes
        @dims = [lat_dim, lon_dim]
        @lat_min = 1/0.0
        @lon_min = 1/0.0
        @lat_max = -1/0.0
        @lon_max = -1/0.0
        #init_grid
    end

    def init_grid
        #puts "init_grid..."
        @ways.each{|id,way|
            #puts "Doing way #{id}"
            way.nodes.each_index.drop(1).each{|i|
                a = way.nodes[i-1].vector
                b = way.nodes[i].vector
                inf = [id, i-1]
                #puts "Doing #{a} - #{b}"
                i = 0
                each_grid(a, b) {|k,square|
                    #puts "Got #{k.inspect}"
                    update_index_bounds(k)
                    square << inf
                }
            }
        }
    end

    def hist
        sizes = Hash.new {|h,k| h[k] = 0 }
        self.each_value.map(&:size).each{|s|
            sizes[s] += 1
        }
        sizes.entries.sort
    end

    def to_js
        keys = @ways.keys.sort
        keyhash = {}
        keys.each_with_index{|k,i| keyhash[k] = i }
        "var LNG_FACTOR = #{LngFactor};\n" +
        "var ways = {" +
            @ways.each.map{|k,v|
                "#{keyhash[k]}:[" +
                    v.nodes.map{|n|
                        "[#{n.vector[0]},#{n.vector[1]}]"
                    }.join(",") +
                "]"
            }.join(",") +
        "};\n" +
        "var grid_dims = [#{@dims[0]}, #{@dims[1]}];\n" +
        "var grid_origin = [#{@origin[0]}, #{@origin[1]}];\n" +
        "var grid_size = [#{@lat_max - @lat_min + 1}, #{@lon_max - @lon_min + 1}];\n" +
        "var grid = {" +
            self.each.flat_map{|k,s|
                if s.size > 0
                    ["'#{k[0]},#{k[1]}':["+
                        s.map{|v|
                            "[#{keyhash[v[0]]},#{v[1]}]"
                        }.join(",") +
                    "]"]
                else
                    []
                end
            }.join(",") +
        "};\n"
    end
end
