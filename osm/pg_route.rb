require 'set'
require 'pg'

class PGRoute

    class Segment
        attr_reader :way_id, :seg_idx, :length, :node_id_1, :node_id_2
        def initialize(db, *args)
            @db = db
            if args.size >= 2
                load_from_ids(args[0], args[1])
            end
        end
        def self.pos_from_ids(db, way_id, seg_idx, param)
            Position.new(Segment.new(db, way_id, seg_idx), param)
        end
        def self.pos_from_wgs(db, pos)
            result = nil
            radius = 50
            begin
                result = db.exec_prepared('get_nearest_segment_info_wgs',
                            ['POINT(%g %g)' % pos.reverse, radius])
                radius *= 2
            end while result.ntuples == 0
            seg = Segment.new(db)
            seg.init_from_hash(result[0])
            Position.new(seg, result[0]['param'].to_f)
        end
        def load_from_ids(way_id, seg_idx)
            @way_id = way_id
            @seg_idx = seg_idx
            res = @db.exec_prepared('get_segment_info', [way_id, seg_idx])[0]
            init_from_hash res
        end
        def init_from_hash(res)
            @way_id    = res['way_id']    || @way_id
            @seg_idx   = res['way_segment_index'] || @seg_idx
            @node_id_1 = res['node_id_1'] || @node_id_1
            @node_id_2 = res['node_id_2'] || @node_id_2
            @oneway    = res['oneway']    || @oneway
            @length    = res['length']    || @length
            @seg_idx = @seg_idx ? @seg_idx.to_i : @seg_idx
            @oneway  = @oneway  ? @oneway.to_i  : @oneway
            @length  = @length  ? @length.to_f  : @length
        end
        def direction_ok?(dir)
            !@oneway || dir == @oneway
        end
        def next(param, dir)
            node_id = param == 0 ? @node_id_1 : @node_id_2
            res = @db.exec_prepared('get_next_segments',
                                    [@way_id, @seg_idx, node_id, dir])
            res.each{|tuple|
                seg = Segment.new(@db)
                seg.init_from_hash(tuple)
                yield(seg, seg.node_id_1 == node_id ? 0 : 1)
            }
        end
        def == other
            @way_id == other.way_id && @seg_idx == other.seg_idx
        end
    end

    Position = Struct.new(:seg, :param)

    def initialize(db)
        @db = db
        @db.prepare('get_segment_info', %q{
            select node_id_1, node_id_2, oneway, length
            from segment
            where way_id = $1 and way_segment_index = $2
        })
        @db.prepare('get_nearest_segment_info_wgs', %q{
            with point_table(point) as
            (
                select st_transform(st_geomfromtext($1, 4326), 927700)
            )
            select way_id, way_segment_index,
                   node_id_1, node_id_2, oneway, length,
                   st_linelocatepoint(linestring, point) as param
            from segment, point_table
            where st_dwithin(point, linestring, $2)
            order by st_distance(point, linestring)
            limit 1
        })
        @db.prepare('get_next_segments', %q{
            select way_id, way_segment_index,
                   node_id_1, node_id_2, oneway, length
            from segment
            where ((node_id_1 = $3 and (oneway is null or oneway = $4))
                or (node_id_2 = $3 and (oneway is null or oneway = -$4)))
            and not (way_id = $1 and way_segment_index = $2)
        })
    end

    def route(pos1, pos2)
        node_ids = route_nodes(pos1, pos2)

        # FIXME: implement
    end

    def route_segs(pos1, pos2)
        if pos1.seg == pos2.seg
            return [[SearchPosition::Tip.new(
                        pos1.seg, pos1.param, pos2.param, nil)], []]
        end
        spos1 = SearchPosition.new(pos1)
        spos2 = SearchPosition.new(pos2, true)
        spos1.partner = spos2
        spos2.partner = spos1
        spos1.start
        spos2.start
        ok = true
        #puts "Entering search loop..."
        while !spos1.result && !spos2.result && ok
            if spos1.radius.infinite? || spos2.radius.infinite?
                # If you've searched an infinite amount and not found
                # a result, there probably isn't one.
                ok = false
                break
            end
            if spos1.radius <= spos2.radius
                #puts "Expanding from source"
                ok = spos1.expand
            else
                #puts "Expanding from destination"
                ok = spos2.expand
            end
        end
        #puts "Exited loop"
        if !ok
            return nil
        end
        (tip1, tip2) = spos1.result || spos2.result.reverse
        [tip1.to_a, tip2.to_a]
    end

    class SearchPosition

        attr_reader :tips, :tips_by_node, :dists, :result
        attr_writer :partner

        def initialize(pos, backwards=false)
            @tips = SortedSet.new
            @unchecked_tips = SortedSet.new
            @dists = {}
            @tips_by_node = {}
            @dir = backwards ? -1 : 1
            @pos = pos
        end

        def start
            if @pos.param == 0 || @pos.seg.direction_ok?(-@dir)
                add_tip Tip.new(@pos.seg, @pos.param, 0, nil)
            end
            if @pos.param == 1 || @pos.seg.direction_ok?(@dir)
                add_tip Tip.new(@pos.seg, @pos.param, 1, nil)
            end
        end

        def check_partner
            new_radius = radius
            got = []
            @unchecked_tips.each{|tip|
                if tip.dist > new_radius
                    break # no use looking any further
                end
                @unchecked_tips.delete(tip) # can check it now
                partner_dist = @partner.dists[tip.end_node]
                if partner_dist
                    got << [tip, partner_dist]
                end
            }
            if got.size == 0
                nil
            else
                tip = got.sort{|a, b| a[0].dist + a[1] <=> b[0].dist + b[1] }.first[0]
                partner_tip = @partner.tips_by_node[tip.end_node]
                if partner_tip.dist > @partner.radius
                    nil
                else
                    @result = [tip, partner_tip]
                end
            end
        end

        def add_tip(tip)
            end_node = tip.end_node
            if end_node
                old_dist = @dists[end_node]
                new_dist = tip.dist
                if !old_dist || new_dist < old_dist
                    @tips << tip
                    @dists[end_node] = new_dist
                    @tips_by_node[end_node] = tip
                    @unchecked_tips << tip

                    if @partner
                        check_partner
                    end
                end
            end
        end

        def expand
            if @tips.size == 0
                return false
            end
            tip = @tips.first
            @tips.delete(tip)
            tip.seg.next(tip.param2, @dir) {|seg, from|
                add_tip Tip.new(seg, from, 1-from, tip)
            }
            true
        end

        def radius
            if @tips.size > 0
                @tips.first.dist
            else
                1/0.0
            end
        end

        class Tip
            attr_reader :seg, :dist, :param1, :param2
            def initialize(seg, param1, param2, prev)
                @seg = seg
                @param1 = param1
                @param2 = param2
                @prev = prev
                @dist = (param2 - param1).abs * @seg.length
                if prev
                    @dist += prev.dist
                end
            end
            def <=> other
                @dist <=> other.dist
            end
            def end_node
                if param2 == 0
                    seg.node_id_1
                elsif param2 == 1
                    seg.node_id_2
                else
                    nil
                end
            end
            def to_a
                a = []
                if @prev
                    a = @prev.to_a
                end
                a << self
            end
        end
    end
end
