require 'matrix'
class PlaneEqn

    attr_reader :vector

    def self.[](vector, dist)
        self.new(vector, dist)
    end

    def initialize(vector, dist)
        @vector = vector
        @dist = dist
    end

    def inspect
        "PlaneEqn[#{@vector}, #{@dist}]"
    end

    def dist(vector=nil)
        if vector
            @vector.inner_product(vector) - @dist
        else
            dist
        end
    end

    def self.segment_equations(seg)
        diff = seg[1] - seg[0]
        len2 = diff.inner_product diff
        len = Math.sqrt(len2)

        para = diff / len2
        para_dist = seg[0].inner_product para

        norm = Matrix[[0,1],[-1,0]] * diff / len
        dist = seg[0].inner_product norm

        [PlaneEqn[norm, dist], PlaneEqn[para, para_dist]]
    end

    def self.point_and_dist(seg, pos)
        (norm_eqn, para_eqn) = segment_equations(seg)
        dist = norm_eqn.dist(pos)
        param = para_eqn.dist(pos)
        if param >= 0 && param <= 1
            [pos - dist * norm_eqn.vector, dist.abs]
        elsif param < 0
            [seg[0], (pos - seg[0]).norm]
        else
            [seg[1], (pos - seg[1]).norm]
        end
    end
end
