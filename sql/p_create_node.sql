create table node (
    node_id text primary key,
    point geometry(POINT, 927700),
    json text,
    filename text,
    block_index int,
    array_index int
);
