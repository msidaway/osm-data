select * from (
    select r1.route_id route_id, r1.run run, r1.sequence sequence1,
           st_distance(s1.point, s2.point) dist
    from route r1 natural join stop s1,
         route r2 natural join stop s2
    where r2.route_id = r1.route_id
      and r2.run = r1.run
      and r2.sequence = r1.sequence + 1
) as dist_tbl where dist > 6000;
