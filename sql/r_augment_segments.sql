alter table segment add node_id_1 text;
alter table segment add node_id_2 text;
alter table segment add oneway int;

begin transaction;

update segment
set node_id_1 = wn1.node_id,
    node_id_2 = wn2.node_id
from way_node wn1, way_node wn2
where wn1.way_id = segment.way_id
  and wn1.way_node_index = segment.way_node_index_1
  and wn2.way_id = segment.way_id
  and wn2.way_node_index = segment.way_node_index_2;

commit;

create index segment_by_node_id_1 on segment (node_id_1);
create index segment_by_node_id_2 on segment (node_id_2);

-- will have to do oneway the "old fashioned" way
