create function get_consecutive_centroids(point geometry, num int)
returns table(dist float, json text)
language plpgsql as $$
-- declare
    -- point geometry = st_transform(st_geomfromtext(geom, 4326), 927700);
begin
    return query
    select point <-> linestring as dist,
           way.json
    from way
    order by point <-> linestring
    limit num;
end;
$$;
