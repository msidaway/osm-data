create table way_sample ( like way );

insert into way_sample
with pt(point) as
    (select ST_Transform(ST_GeomFromText('POINT(-0.12797 51.50777)',
                                         4326),
                         927700))
select way.* from way, pt
where ST_DWithin(linestring, point, 200);
