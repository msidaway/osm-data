create table detail (
    stop_id_1 text,
    stop_id_2 text,
    sequence int,
    way_id text,
    way_segment_index int,
    way_segment_param_1 float,
    way_segment_param_2 float,
    primary key (stop_id_1, stop_id_2, sequence)
);
