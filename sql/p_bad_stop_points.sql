select stop_id, dist from (select stop_id, st_distance(point, st_lineinterpolatepoint(linestring, way_segment_param)) dist from stop natural join segment) as tbl where dist > 20 order by dist desc;
