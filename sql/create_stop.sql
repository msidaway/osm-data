create table stop (
    stop_id text,
    lat number,
    lon number,
    nlat number,
    nlon number,
    dist number,
    way_id text,
    idx int
);
