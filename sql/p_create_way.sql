create table way (
    way_id text primary key,
    linestring geometry(LINESTRING, 927700),
    json text,
    filename text,
    block_index int,
    array_index int
);
