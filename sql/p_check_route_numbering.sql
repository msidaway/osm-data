-- this should return no rows, if the numbering is good
select * from route r
where (
    select count(*) from route
    where route_id = r.route_id
    and run = r.run
    and sequence = r.sequence + 1
) = 0 and (
    select count(*) from route
    where route_id = r.route_id
    and run = r.run
    and sequence > r.sequence
) > 0;
