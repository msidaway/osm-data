create table way_node (
    way_id text,
    way_node_index int,
    node_id text,
    primary key(way_id, way_node_index)
);
