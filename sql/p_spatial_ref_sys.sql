create function insert_ostn02_ntv2() returns void as $$
declare
    rec spatial_ref_sys%rowtype;
begin
    select * from spatial_ref_sys into rec where srid = 27700;
    rec.srid := 927700;
    rec.proj4text := regexp_replace(rec.proj4text, '\+towgs84=[^\s]*',
                                                   '+nadgrids=OSTN02_NTv2.gsb');
    insert into spatial_ref_sys select rec.*;
end;
$$ language plpgsql;

select insert_ostn02_ntv2();
