#!/usr/bin/env ruby

require 'csv'

ids = CSV.parse(File.open("data/bad_stops.csv")).drop(1).map{|entry|
    entry[0]
}

files = Dir.glob('data/bad_stops_latlon_*.csv').sort

i = 0
File.open('data/bad_stops_latlon.csv', 'w') {|out|
    files.each{|fname|
        lines = CSV.parse(File.open(fname))
        header = lines.delete_at(0)
        lines = lines.map{|line|
            vals = line[3].strip.split(/[,\s]+/)
            [vals[0], (vals[1] == "W" ? "-" : "") + vals[2]]
        }.each{|lat, lon|
            out.puts "#{ids[i]},#{lat},#{lon}"
            i += 1
        }
    }
}
