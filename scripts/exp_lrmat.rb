#!/usr/bin/env ruby

require 'csv'

require_relative '../route_detail.rb'

r = RouteDetail.new
r.load_stops_csv
r.load_latlons

good_csv = r.stops_csv.select{|entry| r.latlons.has_key? entry[0] }

eastings = good_csv.map{|entry| entry[4].to_f }
northings = good_csv.map{|entry| entry[5].to_f }
lats = good_csv.map{|entry| r.latlons[entry[0]][0] }
lons = good_csv.map{|entry| r.latlons[entry[0]][1] }

output = [northings, eastings, lats, lons].transpose

CSV.open('data/lrmat.csv', 'w') {|csv|
    output.each{|ent| csv << ent }
}

bll = CSV.parse(File.open('data/bad_stops_latlon.csv'))

bne = {}
r.stops_csv.each{|row|
    bne[row[0]] = row
}

CSV.open('data/blrmat.csv', 'w') {|csv|
    bll.each_with_index{|bllrow,i|
        bnerow = bne[bllrow[0]]
        csv << [bnerow[5], bnerow[4]] + bllrow[1..2]
    }
}
