#!/usr/bin/env ruby

require 'pg'

require_relative '../osm/pg_route'

db = PG::Connection.new(dbname: 'osm')
r = PGRoute.new(db)

db.prepare('insert_detail', %q{
    insert into detail(stop_id_1, stop_id_2, sequence,
        way_id, way_segment_index, way_segment_param_1, way_segment_param_2)
    values ($1, $2, $3, $4, $5, $6, $7)
})

db.exec(%q{
    select distinct
           s1.stop_id stop_id_1, s1.way_id way_id_1, s1.way_segment_index seg_idx_1,
                                                     s1.way_segment_param seg_param_1,
           s2.stop_id stop_id_2, s2.way_id way_id_2, s2.way_segment_index seg_idx_2,
                                                     s2.way_segment_param seg_param_2,
           st_distance(s1.point, s2.point) dist
    from route r1 natural join stop s1,
         route r2 natural join stop s2
    where r1.route_id = r2.route_id
      and r1.run = r2.run
      and r1.sequence + 1 = r2.sequence
      and (select count(*) from detail
           where stop_id_1 = s1.stop_id
           and   stop_id_2 = s2.stop_id) = 0
    order by dist asc
}) {|result|
    count = result.ntuples
    ir = 0
    result.each_row{|row|
        db.exec 'begin transaction'
        begin

        puts "Doing #{ir} out of #{count} (distance #{row[8]}) - stops #{row[0]}, #{row[4]}; segments #{row[1]}/#{row[2]}, #{row[5]}/#{row[6]}"

        pos1 = PGRoute::Segment.pos_from_ids(db, row[1], row[2].to_i, row[3].to_f)
        pos2 = PGRoute::Segment.pos_from_ids(db, row[5], row[6].to_i, row[7].to_f)
        (fsegs, bsegs) = r.route_segs(pos1, pos2)

        i = 0
        fsegs && fsegs.each{|tip|
            db.exec_prepared('insert_detail', [
                row[0], row[4], i, tip.seg.way_id, tip.seg.seg_idx,
                                   tip.param1, tip.param2
            ])
            i += 1
        }
        bsegs && bsegs.reverse.each{|tip|
            db.exec_prepared('insert_detail', [
                row[0], row[4], i, tip.seg.way_id, tip.seg.seg_idx,
                                   tip.param2, tip.param1
            ])
            i += 1
        }

        rescue Exception
            puts 'Rollback'
            db.exec 'rollback'
            raise
        else
            db.exec 'commit'
        end

        ir += 1
    }
}
