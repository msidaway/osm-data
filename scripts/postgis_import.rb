#!/usr/bin/env ruby

require 'pg'
require 'json'
require 'set'
require 'pbf_parser/pbf_parser'

require_relative '../route_detail.rb'
require_relative '../osm/constants.rb'
require_relative '../osm/tags.rb'

class BoundingBox
    LatMile = 1/69.0
    LonMile = 1/43.0

    def initialize(array)
        @lat1 = array[0][0]
        @lat2 = array[0][1]
        @lon1 = array[1][0]
        @lon2 = array[1][1]
    end

    def extend_bounds(miles)
        lat = miles * LatMile
        lon = miles * LonMile
        BoundingBox.new [[@lat1 - lat,
                     @lat2 + lat],
                    [@lon1 - lon,
                     @lon2 + lon]]
    end

    def contains?(obj)
        lat = obj[:lat] || obj['lat'].to_f
        lon = obj[:lon] || obj['lon'].to_f
        lat >= @lat1 &&
        lat <= @lat2 &&
        lon >= @lon1 &&
        lon <= @lon2
    end

    def to_geom
        'POLYGON($1 $2,$3 $2,$3 $4,$1 $4,$1 $2)' % [@lon1, @lat1, @lon2, @lat2]
    end

    def to_a
        [@lon1, @lat1, @lon2, @lat2]
    end
end

class RouteDetail

    include OSMConstants

    PbfFilename = 'data/england-latest.osm.pbf'

    attr_reader :db, :nodeinfo

    def find_tfl_bounds

        #puts LatMile
        #puts LonMile

        minmax = BoundingBox.new(@routes.values.flatten.uniq.map{|node_id| @latlons[node_id].to_a }.transpose.map(&:minmax))

        #p minmax

        minmax = minmax.extend_bounds(1)

        #p minmax

        minmax
    end

    def open_pbf
        if @pbf.nil?
            @pbf = PbfParser.new(PbfFilename)
        end
    end

    def open_db(force=false)
        if @db.nil? || force
            @db = PG::Connection.open(dbname: 'osm')
        end
    end

    def prepare_if_not_exists(name, sql)
        begin
            @db.prepare name, sql
        rescue PG::DuplicatePstatement
            puts "WARNING: prepared statement #{name} already exists. Continuing."
            # not necessary to do rollback - that problem was occurring for a different reason
        end
    end

    def import_nodes
        open_pbf
        open_db
        prepare_if_not_exists 'insert_node', %q{
            insert into node (node_id, point, json,
                              filename, block_index, array_index)
            values ($1, ST_Transform(ST_GeomFromText($2, 4326), 927700),
                    $3, $4, $5, $6)
        }
        bounds = find_tfl_bounds.extend_bounds(5)
        start_block = @db.exec('select max(block_index) from node where filename = $1', [PbfFilename]).getvalue(0, 0)
        if start_block
            start_block = start_block.to_i + 1
        else
            start_block = 0
        end
        (start_block...@pbf.size).each{|block_index|
            @pbf.seek(block_index) or raise "Cannot seek"
            break if @pbf.nodes.size == 0
            $stdout.write "Processing nodes in block #{block_index}..."; $stdout.flush
            count = 0

            @db.exec 'begin'
            begin
                @pbf.nodes.each_with_index{|node, node_index|
                    if bounds.contains?(node)
                        count += 1
                        @db.exec_prepared('insert_node',
                                          [node[:id],
                                           "POINT(#{node[:lon]} #{node[:lat]})",
                                           JSON.generate(node),
                                           PbfFilename,
                                           block_index,
                                           node_index])
                    end
                }
            rescue Exception
                puts "Rolling back transaction"
                @db.exec 'rollback'
                raise
            else
                @db.exec 'commit'
            end
            puts "done (#{count})."
        }
    end

    def check_nodes(a, b)
        open_pbf
        open_db
        bounds = find_tfl_bounds.extend_bounds(5)

        prepare_if_not_exists 'select_node_id', %q{
            select node_id from node where node_id = $1
        }

        total_found = 0; total_missing = 0

        (a..b).each{|block_index|
            $stdout.write "Processing block #{block_index}..."; $stdout.flush
            @pbf.seek(block_index) or raise "Cannot seek to #{block_index}"
            found = 0; missing = 0
            @pbf.nodes.each_with_index{|node, node_index|
                if bounds.contains?(node)
                    rows = @db.exec_prepared('select_node_id', [node[:id]]).values
                    #p rows
                    if rows.size == 0 || rows[0].size == 0 || rows[0][0] != node[:id].to_s
                        missing += 1
                    else
                        found += 1
                    end
                end
            }
            puts "found: #{found}, missing: #{missing}."
            total_found += found
            total_missing += missing
        }
        puts "Total found: #{total_found}, missing: #{total_missing}."
    end

    def load_nodeinfo(force=false)
        if !@nodeinfo || force
            @nodeinfo = {}
            bounds = find_tfl_bounds
            open_db
            @db.exec_params(%q{
                with rt(rect) as
                    (select st_transform(st_makeenvelope($1, $2, $3, $4, 4326),
                                         927700))
                select node_id, st_covers(rect, point) from node, rt
            }, bounds.to_a) {|result|
                i = 0
                result.each_row{|node_id, covers|
                    if (i%1000) == 0
                        puts "#{i} #{covers}"
                    end
                    @nodeinfo[node_id] = (covers == 't')
                    i += 1
                }
            }
        end
    end

    def import_ways
        open_pbf
        open_db
        load_nodeinfo

        prepare_if_not_exists 'select_node_xy', %q{
            select ST_X(point) x, ST_Y(point) y
            from node
            where node_id = $1
        }

        prepare_if_not_exists 'insert_way', %q{
            insert into way (way_id, linestring, json,
                              filename, block_index, array_index)
            values ($1, ST_GeomFromText($2, 927700), $3, $4, $5, $6)
        }

        prepare_if_not_exists 'insert_way_node', %q{
            insert into way_node (way_id, way_node_index, node_id)
            values ($1, $2, $3)
        }

        # N.B. bounds are different from before
        bounds = find_tfl_bounds

        start_block = @db.exec('select max(block_index) from way where filename = $1', [PbfFilename]).getvalue(0, 0)
        if start_block
            start_block = start_block.to_i + 1
        else
            # start in the same block as the last nodes - may be some ways as well
            start_block = @db.exec('select max(block_index) from node where filename = $1', [PbfFilename]).getvalue(0, 0).to_i
        end

        puts "Starting at block #{start_block} out of #{@pbf.size}"

        (start_block...@pbf.size).each{|block_index|
            @pbf.seek(block_index) or raise "Cannot seek"
            break if @pbf.ways.size == 0
            $stdout.write "Processing ways in block #{block_index}"; $stdout.flush
            count = 0
            count_trouble = 0

            @db.exec 'begin'
            begin
                @pbf.ways.each_with_index{|way, way_index|
                    if !way[:tags]['highway']
                        next
                    end
                    process = false
                    all_present = true
                    linestring = ""
                    way[:refs].each_with_index{|node_id, way_node_index|
                        rows = nil
                        node_id = node_id.to_s
                        if !@nodeinfo.has_key?(node_id)
                            all_present = false
                        else
                            if @nodeinfo[node_id]
                                process = true
                            end
                            ninf = @db.exec_prepared('select_node_xy', [node_id])[0]
                            linestring += "#{ninf['x']} #{ninf['y']},"
                        end
                    }
                    if process
                        count += 1
                        trouble = false
                        if !all_present || way[:refs].size == 0
                            linestring = nil
                            if !all_present
                                trouble = true
                                count_trouble += 1
                            end
                        else
                            # N.B. removing trailing comma
                            type = "LINESTRING"
                            if way[:refs].size < 2
                                #type = "POINT" # like, WTF?
                                $stdout.write('?'); $stdout.flush
                                next
                            end
                            linestring = "#{type}(#{linestring[0..-2]})"
                        end

                        @db.exec_prepared('insert_way',
                                          [way[:id],
                                           linestring,
                                           JSON.generate(way),
                                           PbfFilename,
                                           block_index,
                                           way_index])
                        way[:refs].each_with_index{|node_id, way_node_index|
                            @db.exec_prepared('insert_way_node',
                                [way[:id], way_node_index, node_id])
                        }
                        $stdout.write(trouble ? "!" : "."); $stdout.flush
                    end
                }
            rescue Exception
                puts "Rolling back transaction"
                @db.exec 'rollback'
                raise
            else
                @db.exec 'commit'
            end
            puts "done (#{count}, including #{count_trouble} with missing nodes)."
        }
    end

    def tag_ways
        open_db
        @db.exec 'alter table way add relevant boolean' rescue nil
        @db.prepare 'set_way_relevant', %q{
            update way set relevant = TRUE where way_id = $1
        } rescue nil

        @db.exec 'begin transaction'
        begin

        @db.exec('select way_id, json from way') {|result|
            result.each_row{|way_id, json|
                data = JSON.parse(json)
                if data['tags']['highway'].split(/;/).any? {|x|
                    OSMConstants::HighwayTypes.member?(x)
                }
                    # otherwise leave as NULL
                    @db.exec_prepared('set_way_relevant', [way_id])
                end
            }
        }

        ensure
            # partial commits do not matter here
            @db.exec 'commit'
        end
    end

    def import_stops
        open_db
        load_stops_csv

        @db.exec %q{
            create table stop (
                stop_id text primary key,
                public_stop_code text,
                national_stop_code text,
                stop_name text,
                point geometry(POINT, 927700),
                heading float,
                stop_area_code text,
                virtual boolean
            )
        } rescue nil

        @db.prepare('insert_stop', %q{
            insert into stop (stop_id, public_stop_code, national_stop_code,
                              stop_name, point, heading, stop_area_code,
                              virtual)
            values ($1, $2, $3, $4, st_geomfromtext($5, 927700), $6, $7, $8)
        }) rescue nil

        @db.exec 'begin transaction'
        begin

        i = 0
        @stops_csv.map{|row|
            row.map{|val| val == "" ? nil : val }
        }.each{|stop_id, public_stop_code, national_stop_code,
                         stop_name, easting, northing, heading, stop_area_code,
                         virtual|
            if public_stop_code == "NONE"
                public_stop_code = nil
            end
            if national_stop_code == "NONE"
                national_stop_code = nil
            end
            @db.exec_prepared('insert_stop', [
                stop_id, public_stop_code, national_stop_code, stop_name,
                'POINT(%s %s)' % [easting, northing], heading, stop_area_code,
                (virtual.to_i != 0)
            ])
            i += 1
        }

        rescue Exception
            puts "\nRollback"
            @db.exec 'rollback'
            raise
        else
            @db.exec 'commit'
        end

        @db.exec %q{
            update stop
            set point =
            st_geomfromtext('POINT(521939 188174)', 927700)
            where stop_id = '6052'
        }
    end

    def pg_nearest_way_point(pt_expr, params, tables, withex,
                             start=50, factor=Math.sqrt(2))
        dist = start
        p1 = params.size + 1
        result = nil
        begin
            #puts "trying dist = #{dist}"
            stmt = %Q{
                #{withex}
                select segment.way_id as way_id,
                       segment.way_segment_index as way_segment_index,
                       st_linelocatepoint(segment.linestring, #{pt_expr})
                           as way_segment_param
                from #{(['segment'] + tables).join(', ')}
                where st_dwithin(#{pt_expr}, segment.linestring, $#{p1})
                order by st_distance(#{pt_expr}, segment.linestring)
                limit 1
            }
            #puts "#{stmt};"
            result = @db.exec_params(stmt, params + [dist])
            @last_result = result
            dist *= factor
        end while result.ntuples == 0
        puts "dist got as high as #{dist/factor}"
        result.values[0]
    end

    attr_reader :last_result

    def pg_store_nearest_points
        open_db

        @db.exec %q{
            alter table stop add way_id text;
            alter table stop add way_segment_index int;
            alter table stop add way_segment_param float;
        } rescue nil

        @db.prepare('set_stop_wayinfo', %q{
            update stop set way_id = $2,
                            way_segment_index = $3,
                            way_segment_param = $4
            where stop_id = $1
        }) rescue nil

        @db.exec 'begin transaction'
        begin

        @db.exec('select stop_id from stop') {|result|
            result.each_row{|row|
                stop_id = row[0]
                withex = %q{
                    with pt(point) as
                        (select point from stop where stop_id = $1)
                }
                puts "Doing #{stop_id}"
                wayinfo = pg_nearest_way_point('pt.point', [stop_id], ['pt'],
                                               withex)
                raise "Failed" if wayinfo.size != 3
                @db.exec_prepared('set_stop_wayinfo', [stop_id] + wayinfo)
            }
        }

        ensure
            @db.exec 'commit'
        end
    end

    def pg_renumber_route_run(route, run, asc=false)
        open_db

        @db.prepare('update_route_sequence', %q{
            update route set sequence = $4
            where route_id = $1 and run = $2 and sequence = $3
        }) rescue nil

        incr = asc ? 1 : -1

        @db.exec_params(%Q{
            select sequence from route where route_id = $1 and run = $2
            order by sequence #{asc ? 'asc' : 'desc'}
        }, [route, run]) {|result|
            @db.exec 'begin transaction'
            begin
                idx = asc ? 0 : result.ntuples - 1
                result.each_row{|row|
                    sequence = row[0]
                    @db.exec_prepared('update_route_sequence',
                                      [route, run, sequence, idx+1])
                    idx += incr
                }
            rescue Exception
                puts "Rollback"
                @db.exec 'rollback'
                raise
            else
                puts "Commit"
                @db.exec 'commit'
            end
        }
    end

    def pg_import_routes
        open_db
        load_routes

        @db.exec %q{
            create table route (
                route_id text,
                run int,
                sequence float,
                stop_id text,
                primary key (route_id, run, sequence)
            )
        } rescue nil

        @db.prepare('insert_route', %q{
            insert into route(route_id, run, sequence, stop_id)
            values($1, $2, $3, $4)
        }) rescue nil

        @db.exec 'begin transaction'
        begin
        
        @routes_csv.each{|row|
            row[2] = row[2].to_f
            ok = nil
            @db.exec 'savepoint sp'
            begin
                begin
                    ok = @db.exec_prepared('insert_route', row[0...4])
                rescue PG::UniqueViolation
                    row[2] += 0.1
                    puts "Trying #{row[2]}"
                    @db.exec 'rollback to savepoint sp'
                end
            end until ok
            @db.exec 'release savepoint sp'
        }

        rescue Exception
            puts "Rollback"
            @db.exec 'rollback'
            raise
        else
            @db.exec 'commit'
        end

        # deal with a minor data bug
        @db.exec %q{
            update route set sequence = 3.2
            where route_id = '58'
            and run = 2
            and sequence = 3
        }

        pg_renumber_route_run('58', 2)
    end

    def renumber_all_routes
        open_db
        @db.exec(%q{ select distinct route_id, run from route }) {|result|
            result.each_row{|route_id, run|
                pg_renumber_route_run(route_id, run, true)
            }
        }
    end

    def update_segments_with_oneway
        open_db
        @db.prepare('update_segment_oneway', %q{
            update segment set oneway = $2 where way_id = $1
        }) rescue nil
        @db.exec 'begin'
        begin

        @db.exec('select way_id, json from way') {|result|
            result.each_row{|way_id, json|
                data = JSON.parse(json)
                oneway = Tags.fetch(data, 'oneway')
                @db.exec_prepared('update_segment_oneway', [way_id, oneway])
            }
        }

        rescue Exception
            puts "Rollback"
            @db.exec 'rollback'
            raise
        else
            puts "Commit"
            @db.exec 'commit'
        end
    end

end
