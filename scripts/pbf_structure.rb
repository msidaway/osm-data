#!/usr/bin/env ruby

require 'json'
require 'pbf_parser/pbf_parser'

def pbf_info
    pbf = PbfParser.new('data/england-latest.osm.pbf')
    info = []
    totals = [0, 0, 0]
    pbf.each{|nodes, ways, relations|
        rec = [nodes.size, ways.size, relations.size]
        totals = [totals, rec].transpose.map{|a,b| a+b }
        p rec + totals
        info << rec
    }
    ret = { info: info, totals: totals }
    File.open('data/england-latest-info.json', 'w') {|f|
        f.write(JSON.generate(ret))
    }
    ret
end

def pbf_summary(inf)
    inf['info'].chunk{|x|x}.map{|b,l| [l.size, l.first] }
end
