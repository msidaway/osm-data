#!/usr/bin/env ruby

require 'csv'
require_relative '../route_detail.rb'

stops = File.open('data/bus_stops.csv') {|f| CSV.parse(f) }
stops_h = stops.delete_at(0)
stops.delete_at(-1) # spurious byte (0x1a) at EOF

latlons = RouteDetail.load_latlons

bad_stops = stops.select{|stop| !latlons.has_key?(stop[0]) }
puts "#{bad_stops.size} out of #{stops.size} stops don't have lat/lon coordinates"

File.open('data/bad_stops.csv', 'w') {|f|
    f.puts CSV.generate{|csv|
        csv << stops_h
        bad_stops.each{|row|
            csv << row
        }
    }
}

File.open('data/bad_stops_eastnorth.ssv', 'w') {|f|
    f.puts bad_stops.map{|entry| entry[4..5].join(' ') }.join("\n")
}
