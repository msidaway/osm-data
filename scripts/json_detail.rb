#!/usr/bin/env ruby

require 'pg'
require 'json'

structure = Hash.new {|h,k| h[k] = [] }

db = PG::Connection.new(dbname: 'osm')

db.exec(%q{
    select stop_id_1, stop_id_2,
           way_segment_param_1 param1,
           way_segment_param_2 param2,
           st_astext(st_transform(st_linesubstring(linestring,
                                                   least(way_segment_param_1,
                                                         way_segment_param_2),
                                                   greatest(way_segment_param_1,
                                                            way_segment_param_2)),
                                  4326)) linestring
    from detail natural join segment
    order by stop_id_1, stop_id_2, sequence
}) {|result|
    result.each{|tuple|
        $stderr.print "#"; $stderr.flush
        key = tuple['stop_id_1'] + ',' + tuple['stop_id_2']
        linestring = tuple['linestring']
        value = []
        if linestring != "LINESTRING EMPTY"
            coordstr = linestring[/ (?<= \( ) .* (?= \) ) /x]
            # 6 decimal places is to the nearest 10-20 cm
            value = coordstr.split(/[, ]/).map{|x| x.to_f.round(6) }
                            .each_slice(2).map(&:reverse)
        end
        if tuple['param2'].to_f < tuple['param1'].to_f
            value = value.reverse
        end
        if value.size > 0 && value[0] == structure[key][-1]
            value.shift
        end
        structure[key] += value
    }
    $stderr.puts ""
}

#puts JSON.generate(structure)

# Generate binary format - this is rather slow!

index = ""
coords = ""

i = 0
structure.each{|key, value|
    $stderr.print "%"; $stderr.flush
    index += [key, coords.size, value.size].pack('a13L>S>')
    coords += value.flat_map{|coord| [coord[0] - 51.5, coord[1]] }.pack('g*')
    i += 1
}
$stderr.puts ""

print [structure.size].pack('S>') + index + coords
