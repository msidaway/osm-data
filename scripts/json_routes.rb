#!/usr/bin/env ruby

require 'pg'
require 'json'

db = PG::Connection.new(dbname: 'osm')

data = Hash.new{|h,k| h[k] = [] }

db.exec(%q{ select route_id, run, sequence, stop_id from route }) {|result|
    result.each{|tuple|
        $stderr.print("#"); $stderr.flush
        data[tuple['route_id'] + ',' + tuple['run']] << tuple['stop_id']
    }
}
$stderr.puts ""

puts JSON.generate(data)
