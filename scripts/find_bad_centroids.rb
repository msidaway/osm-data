#!/usr/bin/env ruby

require 'pg'

num_iter = ARGV[0].to_i || 10
num_points = ARGV[1].to_i || 10
border_size = ARGV[2].to_i || 1000

db = PG::Connection.new(dbname: 'osm')

#minmaxes = db.exec(%q{
#    select min(st_xmin(linestring)) xmin,
#           min(st_ymin(linestring)) ymin,
#           max(st_xmax(linestring)) xmax,
#           max(st_ymax(linestring)) ymax
#    from way;
#}).values[0].map(&:to_f)

minmaxes = [492895.679352042, 144415.853855254, 568175.572936541, 206739.238359357]

minmaxes[0] -= border_size
minmaxes[1] -= border_size
minmaxes[2] += border_size
minmaxes[3] += border_size

db.prepare('centroid_dists', %q{
    select dist from get_consecutive_centroids(st_geomfromtext($1, 927700), $2)
})

(0...num_iter).each{|i|
    x = minmaxes[0] + (minmaxes[2] - minmaxes[0]) * Random.rand
    y = minmaxes[1] + (minmaxes[3] - minmaxes[1]) * Random.rand
    puts "Iteration #{i}: (#{x} #{y})"
    dists = db.exec_prepared('centroid_dists',
                             ['POINT(%g %g)' % [x, y], num_points])
              .values.flatten.map(&:to_f)
    if dists.size != num_points
        raise "Result size is wrong"
    end
    if dists != dists.sort
        raise "Result is incorrectly ordered"
    end
    #p dists
}
