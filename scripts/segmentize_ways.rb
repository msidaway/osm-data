#!/usr/bin/env ruby

require 'pg'
require 'json'

require_relative 'postgis_import'

module Enumerable
    def segmentize
        result = []
        current = []
        last_node = nil
        each{|*elem|
            if last_node
                current << last_node
                last_node = nil
            end
            current << elem
            node = yield(elem)
            if node && current.size > 1
                result << current
                current = []
                last_node = elem
            end
        }
        if current.size > 0
            result << current
        end
        result
    end
end

class RouteDetail
    attr_reader :waycounts

    def load_waycounts(force=false)
        if !@waycounts || force
            @waycounts = {}
            open_db
            @db.exec(%q{
                select node_id, count(*) from way_node group by node_id
            }) {|result|
                result.each_row{|node_id, count|
                    @waycounts[node_id] = count.to_i
                }
            }
        end
    end

    def segmentize_ways
        open_db
        load_waycounts

        @db.exec(%q{
            create table segment (
                way_id text,
                way_segment_index int,
                way_node_index_1 int,
                way_node_index_2 int,
                linestring geometry(LINESTRING,927700),
                primary key (way_id, way_segment_index)
            )
        }) rescue nil

        db.prepare('insert_segment', %q{
            insert into segment(
                way_id, way_segment_index, way_node_index_1, way_node_index_2,
                linestring
                ) values($1, $2, $3, $4, st_geomfromtext($5, 927700))
        }) rescue nil

        db.exec('begin transaction')

        begin

        # Why does getting the result object take so long?
        # Surely the DB should just return a cursor...?
        puts "Doing select..."
        db.exec(%q{
            select way_id, json, st_astext(linestring) from way
            where linestring is not null
              and relevant = TRUE
        }) {|result|
            i = 0
            puts "Done select, entering loop..."
            result.each_row{|way_id,json,geom|
                data = JSON.parse(json)
                coords = geom[/ (?<= \( ) .* (?= \) ) /x].split(',')
                #puts "#{data['refs'].size} -- #{coords.size}"
                [data['refs'], coords].transpose.each_with_index.segmentize{|nc,idx|
                    (node_id, coords) = nc
                    node_id = node_id.to_s
                    @waycounts[node_id] > 1
                }.each_with_index{|list,segment_index|
                    if list.size > 1
                        db.exec_prepared('insert_segment', [
                            way_id,
                            segment_index,
                            list[0][1],
                            list[-1][1],
                            "LINESTRING(#{
                                list.map{|elem| elem[0][1] }.join(',')
                            })"
                        ])
                    end
                }
                puts "#{i}"
                i += 1
                # limit data loss
                if (i%10000) == 0
                    db.exec('commit')
                    db.exec('begin transaction')
                end
                #break if i == 4
            }
        }

        rescue Exception

        puts "\nRollback"
        db.exec('rollback')
        raise

        else

        db.exec('commit')

        end
    end
end
