#!/usr/bin/env ruby

require 'pg'
require 'json'
require 'set'

require_relative '../osm/constants.rb'

db = PG::Connection.new(dbname: 'osm', host: '/tmp')

i = 0

other_highway_types = Set.new

db.exec('select way_id, json from way') {|result|
    result.each_row{|way_id, json|
        data = JSON.parse(json)
        if !OSMConstants::HighwayTypes.member?(data['tags']['highway'])
            puts "#{way_id}: #{json}"        
            other_highway_types << data['tags']['highway']
            i += 1
        end
    }
}

puts "#{i} unwanted ways"

puts "Types: #{other_highway_types.to_a.join(',')}"
