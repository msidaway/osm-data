function [coeffs, M, northings, eastings, lats, lons] = nell_reg(fname)

    lrmat = csvread(fname)

    northings = lrmat(:,1)
    eastings  = lrmat(:,2)
    lats      = lrmat(:,3)
    lons      = lrmat(:,4)

    z = zeros(rows(lrmat), 1)
    o = ones(rows(lrmat), 1)

    M = [[northings; z], [o; z], [z; eastings], [z; o]]

    latslons = [lats; lons]

    coeffs = M \ latslons

end
