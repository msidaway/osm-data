#!/usr/bin/env ruby

require_relative 'osm/db_ways'
#require_relative 'osm/ways'

#ways = Ways.new
#ways.load_data
ways = DbWays.new

ways.ways.each{|way_id,way|
    way.nodes.each_with_index{|way_node,idx|
        node = ways.nodes[way_node.id]
        if !node
            puts "node for way:#{way_id}:#{idx} is missing"
        end
    }
}

puts "Done."
