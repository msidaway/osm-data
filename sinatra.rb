#!/usr/bin/env ruby

require 'sinatra'
require 'net/http'
require_relative 'osm/db_ways'
require_relative 'osm/ways'
require_relative 'osm/grid'
require_relative 'osm/grid_search'
require_relative 'osm/route'

set :bind, "0.0.0.0"
set :port, 8463

#$ways = Ways.new
#$ways.load_data
#$ways.init_grid
#$grid = $ways.grid

$ways = $grid = DbWays.new

$way_router = Route.new($ways)
$grid_search = GridSearch.new($grid)

get '/' do
	send_file 'public/grid_ajax.html'
end

find_point = proc do |lat,lon|
    point = $grid_search.find_point(Vector[lat.to_f,lon.to_f])
    if point
        point.to_a.join ','
    else
        ""
    end
end

get '/find_point/*,*', &find_point
post '/find_point/*,*', &find_point

find_route = proc do |lat1,lon1,lat2,lon2|
    inf1 = $grid_search.find_point_info(Vector[lat1.to_f, lon1.to_f])
    inf2 = $grid_search.find_point_info(Vector[lat2.to_f, lon2.to_f])
    route = inf1 && inf2 && $way_router.route(inf1, inf2)
    if route
        $way_router.get_points_for_route(inf1[:point], inf2[:point], route)
             .map{|point| point.to_a.join ',' }
             .join ' '
    else
        ""
    end
end

get '/find_route/*,*/*,*', &find_route
post '/find_route/*,*/*,*', &find_route
