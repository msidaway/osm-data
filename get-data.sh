#!/bin/sh

if [ $# = 0 ]; then
    echo "Usage: $0 <api-email-address>"
    exit 1
fi

if [ ! -d data ]; then
    mkdir data
fi

for file in data/{bad,manual}_stops_latlon.csv; do
    if [ ! -f "$file" ]; then
        touch "$file"
    fi
done

wget 'http://www.tfl.gov.uk/tfl/businessandpartners/syndication/feed.aspx?email=$1&feedId=11' -O data/bus_routes.csv
wget "http://www.tfl.gov.uk/tfl/businessandpartners/syndication/feed.aspx?email=$1&feedId=10" -O data/bus_stops.csv

wget 'http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1?StopAlso=true&ReturnList=StopID,Latitude,Longitude' -O data/latlons.json_lines

#wget 'http://api.openstreetmap.org/api/0.6/map?bbox=-0.0225,51.5888,0.012,51.6209' -O data/north.osm
#wget 'http://api.openstreetmap.org/api/0.6/map?bbox=-0.0239,51.5718,-0.00565,51.5936' -O data/south_west.osm
#wget 'http://api.openstreetmap.org/api/0.6/map?bbox=-0.00865,51.5718,0.0096,51.5936' -O data/south_east.osm
