Roads to ignore:

bridleway
crossing
cycleway
footpath
footway
path
pedestrian
steps
track
traffic_signals

Roads to consider:

service
bus_stop
residential
mini_roundabout
motorway_junction
primary
primary_link
secondary
tertiary
trunk
trunk_link
turning_circle
unclassified

