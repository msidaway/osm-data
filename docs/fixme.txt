* Problem sometimes with route starting/ending in same segment (e.g. Woodford New Road)?
* Problem with start/end-point being part of a segment whose end-points are both outside the grid square (e.g. Carr Road)?
* Smoothing? (to prevent it from putting bus stops on side roads)
* Still using wrong coordinate system for route finding...
