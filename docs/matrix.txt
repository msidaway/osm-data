a * northing + b = lat
c * easting + d = lon

Vector[a, b, c, d] * Matrix.columns [[northing, 1, 0, 0], ..., [0, 0, easting, 1], ...]] = Vector[lat, ..., lon, ...]

Vector[lat, ..., lon, ...] * Matrix.columns [[northing, 1, 0, 0], ..., [0, 0, easting, 1], ...]].inv = Vector[a, b, c, d]

Matrix.columns [[northing, 1, 0, 0], ..., [0, 0, easting, 1], ...]] = Matrix[[northing, ..., 0, ...], [1, ..., 0, ...], [0, ..., easting, ...], [0, ..., 1, ...]]

a * M = b
M' * a' = b'
a' = M' \ b'

Performing linear regression has revealed the following outliers:
* 16617 - TfL says 51.334766,-0.269844, OS says 51.350932,-0.147943 (I also noticed this on my all-routes map)
* 6052 - TfL's lat/lons are correct (51.57925,-0.241581), but their northing is wrong (e/n is 521939,999999 instead of 521939,188174)

