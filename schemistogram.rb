#!/usr/bin/env ruby

require 'nokogiri'

class MyDocument < Nokogiri::XML::SAX::Document
    def initialize
        @stack = []
        @paths = Hash.new{|h,k| h[k] = 0 }
    end

    def dump
        ents = @paths.entries.sort{|a,b| a[1] <=> b[1] }
        if ents.size > 0
            digits = 1+Math.log(ents[-1][1], 10).to_i
            ents.each{|keyval|
                puts "%*s : %s" % [digits, keyval[1], keyval[0].inspect]
            }
        end
    end

    def start_element name, attributes = []
        @stack << name
        @paths[Array.new @stack] += 1
    end

    def end_element name
        if @stack[-1] != name
            raise "Unmatched tag - #{@stack.inspect} should end in #{name.inspect}" 
        end
        @stack.pop
    end
end

class Schemistogram
    def main(fname)
        @doc = MyDocument.new
        @parser = Nokogiri::XML::SAX::Parser.new(@doc)
        @parser.parse(File.open(fname))
        @doc.dump
    end
end

if __FILE__ == $0
    Schemistogram.new.main(*ARGV)
end
