#!/usr/bin/env ruby

require 'sinatra'
require 'pg'
require 'json'

require_relative 'osm/pg_route'

set :bind, "0.0.0.0"
set :port, 8465

db = PG::Connection.new(dbname: 'osm')
r = PGRoute.new(db)

searches = {}
routes = {}

db.prepare('segment_points_wgs', %q{
    select st_y(p1) lat1, st_x(p1) lon1, st_y(p2) lat2, st_x(p2) lon2
    from (
        select st_transform(st_lineinterpolatepoint(linestring, $3), 4326) p1,
               st_transform(st_lineinterpolatepoint(linestring, $4), 4326) p2
        from segment
        where way_id = $1
        and way_segment_index = $2
    ) as pointtable
})

def segment_points_wgs(db, seg, param1, param2)
    result = db.exec_prepared('segment_points_wgs',
                [seg.way_id, seg.seg_idx, param1, param2])
    if result.ntuples == 0
        nil
    else
        result.values.first.map(&:to_f).each_slice(2).to_a
    end
end

def tip_segments_wgs(db, tips)
    tips.map{|tip|
        segment_points_wgs(db, tip.seg, tip.param1, tip.param2)
    }
end

get '/' do
    send_file 'public/grid_db_new.html'
end

start_demo = lambda do |latlng|
    latlng = JSON.parse(latlng)
    latlng = [latlng['lat'].to_f, latlng['lng'].to_f]
    cookie = Random.rand.to_s.split('.')[1]
    pos = PGRoute::Segment.pos_from_wgs(db, latlng)
    search = PGRoute::SearchPosition.new(pos)
    searches[cookie] = search
    search.start
    JSON.generate({
        cookie: cookie,
        tips: tip_segments_wgs(db, search.tips),
    })
end

continue_demo = lambda do |cookie, radius|
    search = searches[cookie]
    tips = []
    if search && search.expand
        # N.B. this will need to be filtered to remove ones we already had
        tips = tip_segments_wgs(db, search.tips)
        if search.radius > radius.to_f
            searches.delete(cookie)
        end
    end
    JSON.generate({
        cookie: cookie,
        tips: tips,
    })
end

start_route = lambda do |json|
    json = JSON.parse(json)
    (latlng1, latlng2) = json.map{|latlng| [latlng['lat'].to_f, latlng['lng'].to_f] }
    cookie = Random.rand.to_s.split('.')[1]
    pos1 = PGRoute::Segment.pos_from_wgs(db, latlng1)
    pos2 = PGRoute::Segment.pos_from_wgs(db, latlng2)
    search1 = PGRoute::SearchPosition.new(pos1)
    search2 = PGRoute::SearchPosition.new(pos2, true)
    search1.partner = search2
    search2.partner = search1
    search1.start
    search2.start
    tips = []
    if pos1.seg == pos2.seg
        tips = tip_segments_wgs(db, [PGRoute::SearchPosition::Tip.new(pos1.seg, pos1.param, pos2.param, nil)])
    else
        tips = tip_segments_wgs(db, search1.tips) + tip_segments_wgs(db, search2.tips)
        routes[cookie] = [search1, search2]
    end
    JSON.generate({
        cookie: cookie,
        tips: tips,
    })
end

continue_route = lambda do |cookie|
    tips = []
    result = []
    if routes[cookie]
        (search1, search2) = routes[cookie]
        if search1.result || search2.result
            result = tip_segments_wgs(db, (search1.result || search2.result)
                                            .map(&:to_a).flatten)
            routes.delete cookie
        else
            if search1.radius <= search2.radius
                search1.expand
                tips = tip_segments_wgs(db, search1.tips)
            else
                search2.expand
                tips = tip_segments_wgs(db, search2.tips)
            end
        end
    end
    return JSON.generate({
        cookie: cookie,
        tips: tips,
        result: result,
    })
end

get '/start_demo/*', &start_demo
post '/start_demo/*', &start_demo
get '/continue_demo/*,*', &continue_demo
post '/continue_demo/*,*', &continue_demo

get '/start_route/*', &start_route
post '/start_route/*', &start_route
get '/continue_route/*', &continue_route
post '/continue_route/*', &continue_route
