#!/usr/bin/env ruby

require_relative '../osm/db_ways'
require_relative '../osm/util'

w = DbWays.new

w.db.execute("begin transaction")

w.ways.each{|way_id, way|
    puts "#{way.id}"
    way.nodes.each_cons(2) {|wn1, wn2|
        if wn1.dist.nil?
            #puts "#{wn1.inspect}, #{wn2.inspect}"
            wn1.dist =
              w.nodes[wn2.id].vector.adjusted_norm(w.nodes[wn1.id].vector)
        end
    }
}

w.db.execute("commit")
