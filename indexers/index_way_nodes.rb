#!/usr/bin/env ruby

require 'pbf_parser'
require 'sqlite3'

db = SQLite3::Database.new("data/db.sqlite3")
pbf = PbfParser.new("data/greater-london-latest.osm.pbf")

db.execute(%Q{
    create table if not exists way2nodes (
        way_id text,
        way_node_index int,
        node_id text,
        dist number,
        primary key(way_id, way_node_index)
    )
})

# indices - inefficient
indices = db.prepare("select way_index from way where block_id = ?")
way2nodes = db.prepare("insert into way2nodes (way_id, way_node_index, node_id) values (?, ?, ?)")

start_pos = db.execute("select min(block_id) from way")[0][0]
puts "Starting at #{start_pos}"

(start_pos...pbf.size).each{|block_index|
    puts "Doing block #{block_index} / #{pbf.size}"
    db.execute("begin transaction")
    pbf.seek(block_index) or raise "Failed to seek in PBF"
    result = indices.execute(block_index)
    while row = result.next
        way = pbf.ways[row[0]]
        if !way
            raise "Way not found: #{block_index} / #{row[0]}"
        end
        way[:refs].each_with_index{|node_id, way_node_index|
            way2nodes.execute(way[:id], way_node_index, node_id)
        }
    end
    db.execute("commit")
}
