#!/usr/bin/env ruby

require 'set'
require 'matrix'
require 'gdbm'
require 'sqlite3'

require 'pbf_parser'

require_relative '../osm/ways' # for HighwayTypes
require_relative '../osm/simple_grid'

# Important note: this program assumes that all nodes precede all ways in
# the input file

def gdbm_push(gdbm, key, new_value)
    str = gdbm[key]
    if !str
        #str = "[]"
        str = new_value
    else
        str += "/" + new_value
    end
    #val = eval(str)
    #val << new_value
    #gdbm[key] = val.inspect
    gdbm[key] = str
end

grid = SimpleGrid.new

db = SQLite3::Database.new("data/db.sqlite3")

# node_id => lat_lon (for initial build)
db.execute("create table if not exists node (node_id text primary key, block_id int, node_index int, lat number, lon number)")
node_insert = db.prepare("insert into node values (?, ?, ?, ?, ?)")
node2latlon_get = db.prepare("select lat, lon from node where node_id = ?")

# grid x, y => way, node_index
db.execute("create table if not exists grid2segment (x int, y int, way_id text, node_index int, primary key(x, y, way_id, node_index))")
grid2segment = db.prepare("insert into grid2segment values (?, ?, ?, ?)")

# way_id => filepos ( => node_ids )
db.execute("create table if not exists way (way_id text primary key, block_id int, way_index int)")
way_insert = db.prepare("insert into way values (?, ?, ?)")

# node_id => filepos ( => lat_lon )

#db.execute("pragma journal_mode=memory")

pbf = PbfParser.new("data/greater-london-latest.osm.pbf")

ids = Set.new
nodeinf = {}
wayinf = []

last_time = Time.now.tv_sec
elapsed_time = 0

start_index = 0
(start_index...pbf.size).each{|block_index|
    pbf.seek(block_index) || raise("PbfParser#seek(#{block_index}) failed")
    puts "Block #{block_index} / #{pbf.size} (#{elapsed_time} s)"
    db.execute("begin transaction")
    pbf.nodes.each_with_index{|node, index|
        node_id = node[:id].to_s
        node_insert.execute(node_id, block_index, index, node[:lat], node[:lon])
    }
    puts "Done nodes (#{pbf.nodes.size})"
    skipped_ways = 0
    pbf.ways.each_with_index{|way, way_index|
        if !way[:tags].has_key?("highway") ||
           !Ways::HighwayTypes.member?(way[:tags]["highway"])
            skipped_ways += 1
            next
        end
        pos_str = [block_index, way_index].join(",")
        way_id = way[:id].to_s
        way_insert.execute(way_id, block_index, way_index)
        way[:refs].each_cons(2).each_with_index{|pair, way_node_index|
            (ref1, ref2) = pair
            latlon1 = node2latlon_get.execute(ref1).next
            latlon2 = node2latlon_get.execute(ref2).next
            if !latlon1 || !latlon2
                $stderr.puts "WARNING: nodes #{ref1} and #{ref2} have not yet both been processed"
            else
                latlon1 = Vector.elements latlon1.map(&:to_f)
                latlon2 = Vector.elements latlon2.map(&:to_f)
                grid.each_grid_index(latlon1, latlon2) {|index|
                    (x, y) = index.to_a
                    grid2segment.execute(x, y, way[:id], way_node_index)
                }
            end
        }
    }
    puts "Done ways (#{pbf.ways.size} - skipped #{skipped_ways})"
    db.execute("commit")
    puts "Done commit"
    new_time = Time.now.tv_sec
    elapsed_time = new_time - last_time
    last_time = new_time
}
