#!/usr/bin/env ruby

require 'sqlite3'
require 'pbf_parser'

require_relative '../osm/tags'

db = SQLite3::Database.new("data/db.sqlite3")

db.execute("alter table way add column oneway int")

pbf = PbfParser.new("data/greater-london-latest.osm.pbf")

update = db.prepare("update way set oneway = ? where way_id = ?")

start_pos = db.execute("select min(block_id) from way")[0][0]

(start_pos...pbf.size).each{|block_index|
    puts "Block #{block_index}"
    db.execute("begin transaction")
    pbf.seek(block_index) or raise "Can't seek"
    pbf.ways.each{|way|
        if way[:tags].has_key?('oneway')
            update.execute(Tags.convert('oneway', way[:tags]['oneway']), way[:id])
        end
    }
    db.execute("commit")
}
