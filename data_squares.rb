#!/usr/bin/env ruby

require 'json'
require 'set'

module DataSquares

    @@latlons_file = 'data/latlons.json_lines'

    @@latlons = {}
    File.readlines(@@latlons_file)
        .map{|line| JSON.parse(line) }
        .select{|entry| entry[0] == 0 }
        .each{|entry| @@latlons[entry[1]] = entry[2..3].map(&:to_f) }

    def self.latlons
        @@latlons
    end

end

def calc_squares(latlons,
                 origin=[51.5, 0.0],
                 size=[1.0/69.0, 1.0/43.0],
                 margin=[0.5/69.0, 0.5/43.0],
                 overlap=[0.001/69.0, 0.001/43.0])
    occupied = Set.new
    margin_rel = [margin, size].transpose.map{|x,y| x/y }
    actual_size = [size, overlap.map{|x| 2*x }].transpose.map{|x,y| x - y }
    latlons.each{|lat,lon|
        x0 = (lat - origin[0]) / actual_size[0]
        y0 = (lon - origin[1]) / actual_size[1]
        x1 = (x0 - margin_rel[0]).floor
        y1 = (y0 - margin_rel[1]).floor
        x2 = (x0 + margin_rel[0]).floor
        y2 = (y0 + margin_rel[1]).floor
        (x1..x2).each{|x|
            (y1..y2).each{|y|
                occupied << [x, y]
            }
        }
    }
    occupied
end
