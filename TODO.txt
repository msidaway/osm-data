* If any other segments are within 150% (or some appropriate such figure) of
  the distance of the closest, take these along with the closest, rank them
  according to road type (in particular, putting other road types before
  service roads), and pick the best by (rank, distance).
    * Perhaps examine the data directly.
    * Then re-do the route detail around these stops.
