#!/usr/bin/env ruby

require 'json'
require 'csv'
require 'matrix'
require 'fileutils'

require_relative 'osm/grid_search'
require_relative 'osm/route'
require_relative 'osm/db_ways'
require_relative 'osm/ways'

class RouteDetail

    class Error < Exception; end
    class NoLatLonError < Error
        def initialize(id)
            super("No lat/lon for #{id}")
        end
    end
    class BadRouteNameError < Error
        def initialize(rtname)
            super("Bad route name: #{rtname}")
        end
    end

    RoutesFile = 'data/bus_routes.csv'
    StopsFile = 'data/bus_stops.csv'
    LatLonsFile = 'data/latlons.json_lines'
    BadLatLonFiles = ['data/bad_stops_latlon.csv',
                      'data/manual_stops_latlon.csv']
    Bounds = [51.28554, 51.69344, -0.511482, 0.335437]

    attr_reader :routes, :latlons, :ways, :grid, :way_router, :grid_search, :detail,
                :nearest, :routes_csv, :routes_h, :stops_csv, :stops_h

    def db_ways= dbw
        @ways = @grid = dbw
        @way_router = Route.new @ways
        @grid_search = GridSearch.new @grid
    end

    def load_stops_csv(force=false)
        if !@stops_csv || force
            csv = CSV.parse(File.open(StopsFile))
            @stops_h = csv.delete_at(0)
            csv.delete_at(-1)
            @stops_csv = csv
        end
    end

    def load_routes(force=false)
        if !@routes_csv || force
            STDERR.puts "Loading #{RoutesFile}..."

            csv = CSV.parse(File.open(RoutesFile))
            @routes_h = csv.delete_at(0)
            csv.delete_at(-1) # discard bad entry
            @routes_csv = csv

            routes = csv.map{|entry| [entry[0], entry[1].to_i, entry[2].to_i, entry[3]] }
                        .sort
                        .group_by{|entry| entry[0..1] }

            routes.each{|key,array|
                routes[key] = array.map{|elem| elem[3] } # extract stop IDs
            }

            #puts "routes.keys = #{routes.keys.inspect}"

            @routes = routes

            #STDERR.puts route.map{|entry| entry.join(',') }.join("\n")
        end
    end

    def load_latlons(force=false)
        if !@latlons || force
            STDERR.puts "Loading #{LatLonsFile}..."

            latlons = {}
            File.readlines(LatLonsFile)
                .map{|line| JSON.parse(line) }
                .select{|entry| entry[0] == 0 }
                .each{|entry| latlons[entry[1]] = Vector.elements(entry[2..3].map(&:to_f)) }

            BadLatLonFiles.each{|fname|
                CSV.open(fname) {|csv|
                    csv.each{|row|
                        latlons[row[0]] = row[1..2].map(&:to_f)
                    }
                }
            }

            @latlons = latlons
        end
    end

    def self.find_detail(routes, latlons, way_router, grid_search)
        STDERR.puts "Finding road points and routes..."

        detail = Hash.new {|h,k| h[k] = {} }
        routes.each{|key,route|
            (route_name,dir) = key
            if detail[route_name] == false
                next
            end
            begin
                STDERR.puts "#{route_name}/#{dir}"
                detail[route_name][dir] = route.map{|id|
                    gpos = latlons[id]
                    if !gpos
                        raise NoLatLonError.new(id)
                    end
                    grid_search.find_point_info(gpos, 100)
                }.each_cons(2).map{|a,b|
                    way_router.route_points(a, b, 2000)
                }
            rescue Exception => e
                STDERR.puts "Abandoning route #{route_name} - #{e.inspect}"
                detail[route_name] = false
            end
        }

        detail.select {|k,v| v != false }
    end

    def init(use_files=false)

        load_routes
        load_latlons
    
        if use_files
            @ways = Ways.new
            STDERR.puts "Loading OSM data files..."
            @ways.load_data
            STDERR.puts "Initialising grid..."
            @ways.init_grid
            @grid = ways.grid

            # keep only the ones we actually have the data for
            keep = [['357', 1], ['357', 2]]
            routes.keep_if{|k,v| keep.member?(k) }

        else
            @ways = @grid = DbWays.new
        end

        STDERR.puts "routes.size = #{routes.size}"

        @way_router = Route.new(@ways)
        @grid_search = GridSearch.new(@grid)

        @nearest = {}
    end

    def find_nearest_points
        pct = 100.0 / @latlons.size
        num = @latlons.size / 20
        i = 0
        @latlons.each{|stop_id, latlon|
            if i % num == 0
                $stderr.puts "\n#{i*pct}"
            end
            i += 1
            if latlon[0] < Bounds[0] || latlon[0] > Bounds[1] ||
               latlon[1] < Bounds[2] || latlon[1] > Bounds[3]
                $stderr.write "!"; $stderr.flush
                next
            end
            if @nearest.has_key? stop_id
                $stderr.write "."; $stderr.flush
                next
            end
            @inf = grid_search.find_point_info(latlon)
            @point = @inf[:point]
            #p @point, latlon
            @nearest[stop_id] = { :inf => @inf,
                                  :dist => (@point - latlon).norm }
            $stderr.write "#"; $stderr.flush
        }
    end

    def save_stops
        ways.db.execute("begin transaction")
        stmt = ways.db.prepare("insert into stop values(?, ?, ?, ?, ?, ?, ?, ?)")
        @latlons.each{|stop_id, latlon|
            vals = @nearest[stop_id]
            vals ||= { :inf => { :way_id => nil, :idx => nil, :point => Vector[nil, nil] },
                       :dist => nil }
            stmt.execute(stop_id, latlon[0], latlon[1], vals[:inf][:point][0], vals[:inf][:point][1], vals[:dist], vals[:inf][:way_id], vals[:inf][:idx])
        }
        ways.db.execute("commit")
    end

    def load_stops
        ways.db.execute("select stop_id, way_id, idx, nlat, nlon, dist from stop") {|row|
            @nearest[row[0]] = { :inf => { :way_id => row[1], :idx => row[2],
                                           :point => Vector[row[3], row[4]] },
                                 :dist => row[5] }
        }
    end

    def find_route_detail
        @route_detail ||= {}
        pct = 100.0 / @routes.size
        num = @routes.size / 20
        i = 0
        stmt = ways.db.prepare("insert into detail (stop1, stop2, node_list) values (?, ?, ?)")
        @routes.each{|key, array|
            if i % num == 0
                $stderr.puts "\n#{i*pct}"
            end
            i += 1
            $stderr.write key.join(","); $stderr.flush
            ways.db.execute("begin transaction")
            array.each_cons(2) {|pair|
                if !@nearest[pair[0]] || !@nearest[pair[1]]
                    $stderr.write "!"; $stderr.flush
                    next
                end
                if @route_detail.has_key? pair
                    $stderr.write "."; $stderr.flush
                    next
                end
                @route_detail[pair] = @way_router.route(@nearest[pair[0]][:inf],
                                                        @nearest[pair[1]][:inf])
                stmt.execute(pair[0], pair[1], @route_detail[pair].join(","))
                $stderr.write "#"; $stderr.flush
            }
            ways.db.execute("commit")
            $stderr.write "/"; $stderr.flush
        }
    end

    def process_data(routes = @routes, fname)
        @detail = self.class.find_detail(routes, @latlons, @way_router, @grid_search)

        self.class.finalise_data(@detail, routes)

        STDERR.puts "Writing output file..."

        File.open(fname, 'w') {|f|
            # fast_generate is safe because we know there are no loops
            f.puts JSON.fast_generate(@detail)
        }

        STDERR.puts "Finished!"
    end

    def process_data_seq(route_names = nil)
        if route_names.nil? or route_names.empty?
            route_names = @routes.each_key.group_by {|a,b| a}.keys
        end
        FileUtils.mkdir_p('out/detail')
        route_names.each {|name|
            raise BadRouteNameError.new(name) if name !~ /^[A-Za-z0-9]+$/
            fname = "out/detail/#{name}.json"
            if File.exist?(fname)
                STDERR.puts "Already done #{name} - skipping"
                next
            end
            routes = @routes.select {|key,value| key[0] == name }
            if routes.size > 0
                process_data(routes, fname)
            end
        }
    end

    def find_routes(use_files=false, route_names)
        init(use_files)
        process_data_seq(route_names)
    end

    def retry
        @detail = self.class.find_detail(@routes, @latlons, @way_router, @grid_search)
    end

    def rough_json
        JSON.generate(@routes.flat_map{|key,route|
            route.map{|stop|
                @latlons[stop] && @latlons[stop].to_a
            }.chunk{|x| !x.nil? }.select{|x,y| x && y.size > 1 }.map{|x,y| y }
        })
    end

    def self.finalise_data(detail, routes)
        STDERR.puts "Checking and finalising data..."

        detail.each{|route_name,h|
            h.each{|dir,runs|
                STDERR.puts "#{route_name}/#{dir}"
                if runs.empty?
                    STDERR.puts "runs is empty - abandoning this route run"
                    h.delete(dir)
                    next
                end
                runs.each_cons(2) {|x,y|
                    if x[-1] != y[0]
                        STDERR.puts "WARNING: adjacent runs for #{route_name}/#{dir} do not agree on mutual end-point"
                    end
                }
                stops = [0]
                i = 0
                runs.each{|run|
                    i += run.size-1
                    stops << i
                }
                h[dir] = {
                    :stops => stops,
                    :points => runs.inject{|x,y| x + y.drop(1) }.map(&:to_a),
                    :stop_ids => routes[[route_name,dir]],
                }
            }
        }
    end

    def self.main()
        use_files = ARGV.delete('-f')
        self.new.find_routes(use_files, ARGV)
    end

    self.main() if __FILE__ == $0

end
