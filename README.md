To get limited data (just one bus route)
========================================

    ./get-data.sh
    mkdir out
    ./route_detail.rb -f

To get the full data (Greater London)
=====================================

Dependencies
------------

* protobuf-c libs + headers (https://github.com/protobuf-c/protobuf-c) (to compile pbf_parser's native component)
* pbf_parser Ruby gem (gem install pbf_parser) (https://github.com/planas/pbf_parser)

Code
----

    wget http://download.geofabrik.de/europe/great-britain/england/greater-london-latest.osm.pbf -O data/greater-london-latest.osm.pbf
    indexers/make_index.rb
    indexers/add_fields.rb
    indexers/index_way_nodes.rb
    indexers/find_dists.rb
    sqlite3 data/db.sqlite3 '.read sql/create_node_ways_index.sql'
    ./route_detail.rb # or ./run-batch.sh

Output can be found in out/detail/*.json
