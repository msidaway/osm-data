#!/usr/bin/env ruby

require 'sqlite3'
require 'pbf_parser'

restarts = [3, 332, 335]

db = SQLite3::Database.new("data/db.sqlite3")

pbf = PbfParser.new("data/greater-london-latest.osm.pbf")

nn = 0
nw = 0
nr = 0
(0...pbf.size).each{|block_index|
    pbf.seek(block_index)
    nn += pbf.nodes.size
    nw += pbf.ways.size
    nr += pbf.relations.size
    puts "#{pbf.nodes.size} #{pbf.ways.size} #{nn} #{nw}"
    if pbf.ways.size == 0 && (rows = db.execute("select * from way where block_id = ?", block_index)).size != 0
        puts "Got #{rows.size} ways where they don't belong (in block #{index})"
    end
}
